# STATIC-PRO microbiome-study analysis code

[![CC BY 4.0][cc-by-shield]][cc-by]

[cc-by]: http://creativecommons.org/licenses/by/4.0/
[cc-by-shield]: https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg

_Authors_: Fien (G.J.) van Beveren/Wouter A.A. de Steenhuijsen Piters <a href="https://orcid.org/0000-0002-1144-4067" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a>  
_Date_: 6-1-2022

This code accompanies the manuscript '_Nasopharyngeal microbiota in children is associated with severe asthma exacerbations._'.

## Description

This repository contains the Rmd-scripts ([`scripts/`](scripts/)) and functions ([`src/`](src/)) used to generate results ([`results/`](results/)). The aim of this project was to study upper respiratory tract microbiota in children admitted to hospital with an asthma exacerbation. 

Raw microbiota data were made publicly available PRJNA911564, including minimal participant metadata. Formatted input-files are available upon request (please contact w.a.a.de_steenhuijsen_piters@lumc.nl).  

The included code was ran in R version 4.1.2

## Main scripts

The main scripts include scripts to perform quality control of the samples and scripts to perform microbiome analyses. \
Important scripts for quality control include decontam.Rmd (for decontamination of the samples) and MUIS_vs_MOL.Rmd (where we inspect differences in microbiota between the 2 studies used as our healthy controls). \
Important scripts for research question 1 include scripts under `SAA vs HC` and `differential abundance` and `cluster_analysis_statistics.Rmd`. \
Important scripts for research question 2 include scripts under `IC vs MC`,  `differential abundance` and `cluster_analysis_statistics.Rmd`.  \
Important scripts for research question 3 include scripts under `Objective 3`

## Dependencies

R version 4.1.2 (2021-11-01) & RStudio version 2021.09.1 
Platform: x86_64-apple-darwin17.0 (64-bit)
Running under: macOS Mojave 10.14.6

Matrix products: default
BLAS:   /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libBLAS.dylib
LAPACK: /Library/Frameworks/R.framework/Versions/4.1/Resources/lib/libRlapack.dylib

Main packages: 
tidyverse_1.3.1

phyloseq_1.38.0: for installation instructions go tohttps://joey711.github.io/phyloseq/install.html
 - please note that it is advisable to first install BiocManager when installing the phyloseq package, for instructions see https://www.bioconductor.org/packages/release/bioc/html/phyloseq.html
 - On this site you can also find what version of phyloseq and BiocManager you need for your R version. 

Other packages: 
locale:
[1] nl_NL.UTF-8/nl_NL.UTF-8/nl_NL.UTF-8/C/nl_NL.UTF-8/nl_NL.UTF-8

attached base packages:
[1] stats     graphics  grDevices utils     datasets  methods   base     

other attached packages:
 [1] TempPackage_1.0 docstring_1.0.0 styler_1.7.0    lintr_2.0.1     ggpubr_0.4.0   
 [6] phyloseq_1.38.0 glue_1.6.2      here_1.0.1      magrittr_2.0.2  forcats_0.5.1  
[11] stringr_1.4.0   dplyr_1.0.8     purrr_0.3.4     readr_2.1.2     tidyr_1.2.0    
[16] tibble_3.1.6    ggplot2_3.3.5   tidyverse_1.3.1

loaded via a namespace (and not attached):
  [1] colorspace_2.0-3       ggsignif_0.6.3         ellipsis_0.3.2        
  [4] rprojroot_2.0.2        markdown_1.1           XVector_0.34.0        
  [7] fs_1.5.2               gridtext_0.1.4         ggtext_0.1.1          
 [10] rstudioapi_0.13        farver_2.1.0           roxygen2_7.1.2        
 [13] remotes_2.4.2          fansi_1.0.3            lubridate_1.8.0       
 [16] xml2_1.3.3             R.methodsS3_1.8.1      codetools_0.2-18      
 [19] splines_4.1.2          knitr_1.38             pkgload_1.2.4         
 [22] ade4_1.7-18            jsonlite_1.8.0         broom_0.7.12          
 [25] cluster_2.1.2          dbplyr_2.1.1           R.oo_1.24.0           
 [28] compiler_4.1.2         httr_1.4.2             backports_1.4.1       
 [31] lazyeval_0.2.2         assertthat_0.2.1       Matrix_1.4-1          
 [34] fastmap_1.1.0          cli_3.2.0              htmltools_0.5.2       
 [37] tools_4.1.2            igraph_1.2.11          gtable_0.3.0          
 [40] GenomeInfoDbData_1.2.7 reshape2_1.4.4         Rcpp_1.0.8.3          
 [43] carData_3.0-5          Biobase_2.54.0         cellranger_1.1.0      
 [46] vctrs_0.3.8            Biostrings_2.62.0      rhdf5filters_1.6.0    
 [49] multtest_2.50.0        ape_5.6-2              nlme_3.1-157          
 [52] iterators_1.0.14       xfun_0.30              brio_1.1.3            
 [55] ps_1.6.0               testthat_3.1.2         rvest_1.0.2           
 [58] lifecycle_1.0.1        rstatix_0.7.0          zlibbioc_1.40.0       
 [61] MASS_7.3-56            scales_1.1.1           hms_1.1.1             
 [64] rex_1.2.1              parallel_4.1.2         biomformat_1.22.0     
 [67] xmlparsedata_1.0.5     rhdf5_2.38.1           RColorBrewer_1.1-2    
 [70] yaml_2.3.5             stringi_1.7.6          desc_1.4.1            
 [73] S4Vectors_0.32.4       foreach_1.5.2          permute_0.9-7         
 [76] BiocGenerics_0.40.0    cyclocomp_1.1.0        GenomeInfoDb_1.30.1   
 [79] rlang_1.0.2            pkgconfig_2.0.3        bitops_1.0-7          
 [82] evaluate_0.15          lattice_0.20-45        Rhdf5lib_1.16.0       
 [85] labeling_0.4.2         tidyselect_1.1.2       processx_3.5.3        
 [88] plyr_1.8.7             R6_2.5.1               IRanges_2.28.0        
 [91] generics_0.1.2         DBI_1.1.2              pillar_1.7.0          
 [94] haven_2.4.3            withr_2.5.0            mgcv_1.8-39           
 [97] survival_3.3-1         abind_1.4-5            RCurl_1.98-1.6        
[100] modelr_0.1.8           crayon_1.5.1           car_3.0-12            
[103] utf8_1.2.2             tzdb_0.2.0             rmarkdown_2.13        
[106] grid_4.1.2             readxl_1.3.1           data.table_1.14.2     
[109] callr_3.7.0            vegan_2.5-7            reprex_2.0.1          
[112] digest_0.6.29          R.cache_0.15.0         R.utils_2.11.0        
[115] stats4_4.1.2           munsell_0.5.0 