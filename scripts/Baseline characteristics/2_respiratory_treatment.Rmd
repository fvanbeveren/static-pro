---
title: "Untitled"
author: "Fien van Beveren"
date: "2023-12-06"
output: html_document
---

# Load packages

```{r}
library(tidyverse); library(magrittr); library(here); library(glue); 
library(phyloseq); library(ggforce)
library(cowplot); library(haven); library(labelled)
library(gdata); library(knitr); library(vegan); library(ggrepel); 
library(car); library(fpc); library(ggdendro); library(dendextend)
library(RColorBrewer); library(ggpubr)
library(rstatix); library(factoextra); library(forestmodel); library(patchwork); library(xlsx); library(lmtest); library(gridExtra); library(ggstats); library(tableone); library(Maaslin2); library(ggtext)

subdir_name <- "2_respiratory_treatment"

# set paths
knitr::opts_knit$set(root.dir=".", aliases=c(h = "fig.height", w = "fig.width", ow = "out.width"))
knitr::opts_chunk$set(dev=c('png', 'pdf'), 
                      fig.path=here("results", "figures", glue("{subdir_name}/")), dpi=300)
theme_set(theme_light()) 
```

# Load functions

```{r}
source(here("src", "utils.R")) # please see the src-folder for some custom functions used in this script.
source(here("src", "load_dada.R"))
source(here("src", "colour_scheme.R"))
source(here("src", "colour_scheme_2.R"))
source(here::here("src", "calc_ellipses.R"))
source(here::here("src", "pcoa_veg.R"))
source(here::here("src", "plot_pcoa.R"))
```

# Load data
```{r}
ps <- readRDS(here::here("data", "processed", "ps_comb.Rds"))
ps_RA <- ps %>% to_RA() 
```

# Objectives
1. Disease phenotype: 
A) counts of 2-4 year olds and >4 year olds to baseline table
B) 2-4 year old cases vs 2-4 year old controls
  - alpha
  - beta
  - maaslin2
C) 4-6 year old cases vs 4-6 year old controls
  - alpha
  - beta
  - maaslin2
  
#.
# A) counts of 2-4 year olds and >4 year olds to baseline table
## Prepare data

```{r}
meta <- ps %>% 
  meta_to_df() %>% 
  mutate(age_cat = case_when(Age_years < 5 ~ "2-4 years",
                             Age_years > 4 ~ "> 4 years"))

meta_case <- meta %>% filter(study == "PICU")
meta_strat <- meta %>% filter(Age_years <7)
```

## Baseline table MCU vs ICU

```{r}
chisq.test(meta_case$Ward, meta_case$age_cat)$expected # assumptions chi-square are met. 
table(meta_case$Ward, meta_case$age_cat) # assumptions fisher's exact are met

var <- c("age_cat") 

tab_case <- CreateTableOne(vars = var, strata = "Ward", data = meta_case)
print(tab_case, showAllLevels = F)
```

## Baseline table case vs controls (stratified)
```{r}
chisq.test(meta_strat$environment, meta_strat$age_cat)$expected # assumptions chi-square are met. 
table(meta_strat$environment, meta_strat$age_cat) # assumptions fisher's exact are met

var <- c("age_cat") 

tab_strat <- CreateTableOne(vars = var, strata = "environment", data = meta_strat)
print(tab_strat, showAllLevels = F)
```
#.
# B) 2-4 year old cases vs 2-4 year old controls

## Prepare data
```{r}
sample_data(ps) <-  meta %>% column_to_rownames("#SampleID")

ps_strat_young <- ps %>% subset_samples(age_cat == "2-4 years") %>% pres_abund_filter(pres = 1, abund = 0)

meta_strat_young <- ps_strat_young %>% meta_to_df()
```

# B.1) Alpha diversity
### Calculate measures
```{r}
meta_strat_young$shannon <- estimate_richness(ps_strat_young, measure = "Shannon")
meta_strat_young$shannon <- unlist(meta_strat_young$shannon)
meta_strat_young$observed <- estimate_richness(ps_strat_young, measure ="Observed")
meta_strat_young$observed <- unlist(meta_strat_young$observed)

meta_strat_young %<>% 
  mutate(pielou = shannon/log(observed)) %>%
  column_to_rownames("#SampleID")

#assign new metadata with alpha div. measures to phyloseq
sample_data(ps_strat_young) <- meta_strat_young
```

### Visualization 
#### 3 groups
```{r}
boxplot_shannon <- meta_strat_young %>%
  ggplot(aes(x=subject_type, y=shannon, color = subject_type, fill =subject_type)) +
  geom_jitter(alpha = 1, width = 0.2) +
  geom_boxplot(alpha = 0.5, outlier.colour = NA) +
  scale_colour_manual(values = cols2$subject_type) +
  scale_fill_manual(values = cols2$subject_type) +
  labs(fill = "Study group", color = "Study group") +
  theme(axis.title.x = element_blank())

boxplot_shannon + stat_compare_means(method = "anova")

boxplot_observed <- meta_strat_young %>%
  ggplot(aes(x=subject_type, y=observed, color = subject_type, fill = subject_type)) +
  geom_jitter(alpha = 1, width = 0.2) +
  geom_boxplot(alpha = 0.5, outlier.colour = NA) +
  scale_colour_manual(values = cols2$subject_type) +
  scale_fill_manual(values = cols2$subject_type) +
  labs(fill = "Study group", color = "Study group") +
  theme(axis.title.x = element_blank())

boxplot_observed + stat_compare_means(method = "kruskal")

boxplot_pielou <- meta_strat_young %>%
  ggplot(aes(x=subject_type, y=pielou, color = subject_type, fill = subject_type)) +
  geom_jitter(alpha = 1, width = 0.2) +
  geom_boxplot(alpha = 0.5, outlier.colour = NA) +
  scale_colour_manual(values = cols2$subject_type) +
  scale_fill_manual(values = cols2$subject_type) +
  labs(fill = "Study group", color = "Study group") +
  theme(axis.title.x = element_blank())

boxplot_pielou + stat_compare_means(method = "anova")

my_comparisons <- list( c("control", "case (MC)"), c("control", "case (ICU)"))

boxplot_shannon_sign <- boxplot_shannon +
    stat_compare_means(comparisons = my_comparisons, label = "p.signif", method = "t.test", ref.group = "control") +
  stat_compare_means(method = "anova", label.y = 5)  

boxplot_observed_sign <- boxplot_observed +
  stat_compare_means(comparisons = my_comparisons, label = "p.signif", method = "t.test", ref.group = "control") +
  stat_compare_means(method = "anova", label.y = 250)  

boxplot_pielou_sign <- boxplot_pielou +
  stat_compare_means(method = "anova", label.y = 0.9)  

p_pielou <- plot_grid(boxplot_shannon_sign + 
                    theme(legend.position = "none", axis.text.x = element_text(angle = 45, hjust = 1)) +
                    labs(y = "Shannon diversity index", subtitle = "A"),
                  boxplot_observed_sign + 
                    theme(legend.position = "none", axis.text.x = element_text(angle = 45, hjust = 1)) +
                    labs(y = "Number of observed species", subtitle = "B"),
                  boxplot_pielou_sign +
                   theme(legend.position = "none", axis.text.x = element_text(angle = 45, hjust = 1)) +
                    labs(y = "Pielou's evenness", subtitle = "C"),
                  ncol=3, rel_widths = c(0.5, 0.5, 0.5), align="v", axis="lr") 
p_pielou
```
#### 2 groups
```{r}
boxplot_shannon_case_contr <- meta_strat_young %>%
  ggplot(aes(x=environment, y=shannon, color = environment, fill =environment)) +
  geom_jitter(alpha = 1, width = 0.2) +
  geom_boxplot(alpha = 0.5, outlier.colour = NA) +
  scale_colour_manual(values = cols2$environment) +
  scale_fill_manual(values =cols2$environment) +
  labs(subtitle = "Shannon diversity index in controls vs cases")

boxplot_shannon_case_contr + stat_compare_means(method ="anova")

boxplot_obs_case_contr <- meta_strat_young %>%
  ggplot(aes(x=environment, y=observed, color = environment, fill = environment)) +
  geom_jitter(alpha = 1, width = 0.2) +
  geom_boxplot(alpha = 0.5, outlier.colour = NA) +
  scale_colour_manual(values = cols2$environment) +
  scale_fill_manual(values =cols2$environment) +
  labs(subtitle="Richness in controls vs cases")

boxplot_obs_case_contr + stat_compare_means(method ="anova")

boxplot_pielou_case_contr <- meta_strat_young %>%
  ggplot(aes(x=environment, y=pielou, color = environment, fill = environment)) +
  geom_jitter(alpha = 1, width = 0.2) +
  geom_boxplot(alpha = 0.5, outlier.colour = NA) +
  scale_colour_manual(values = cols2$environment) +
  scale_fill_manual(values =cols2$environment) +
  labs(subtitle="Evenness in controls vs cases")

boxplot_pielou_case_contr + stat_compare_means(method ="anova")
```

#### MC-IC
```{r}
boxplot_shannon <- meta_strat_young %>%
  filter(study == "PICU") %>% 
  ggplot(aes(x=Ward, y=shannon, color = Ward, fill =Ward)) +
  geom_jitter(alpha = 1, width = 0.2) +
  geom_boxplot(alpha = 0.5, outlier.colour = NA) +
  scale_colour_manual(values = cols2$ward_3) +
  scale_fill_manual(values = cols2$ward_3) +
  labs(fill = "Ward", color = "Ward") +
  theme(axis.title.x = element_blank())

boxplot_shannon + stat_compare_means(method = "anova")

boxplot_observed <- meta_strat_young %>%
  filter(study == "PICU") %>% 
  ggplot(aes(x=Ward, y=observed, color = Ward, fill =Ward)) +
  geom_jitter(alpha = 1, width = 0.2) +
  geom_boxplot(alpha = 0.5, outlier.colour = NA) +
  scale_colour_manual(values = cols2$ward_3) +
  scale_fill_manual(values = cols2$ward_3) +
  labs(fill = "Ward", color = "Ward") +
  theme(axis.title.x = element_blank())

boxplot_observed + stat_compare_means(method = "kruskal")

boxplot_pielou <- meta_strat_young %>%
  filter(study == "PICU") %>% 
  ggplot(aes(x=Ward, y=pielou, color = Ward, fill =Ward)) +
  geom_jitter(alpha = 1, width = 0.2) +
  geom_boxplot(alpha = 0.5, outlier.colour = NA) +
  scale_colour_manual(values = cols2$ward_3) +
  scale_fill_manual(values = cols2$ward_3) +
  labs(fill = "Ward", color = "Ward") +
  theme(axis.title.x = element_blank())

boxplot_pielou + stat_compare_means(method = "anova")

my_comparisons <- list( c("control", "case (MC)"), c("control", "case (ICU)"))

boxplot_shannon_sign <- boxplot_shannon +
    stat_compare_means(comparisons = my_comparisons, label = "p.signif", method = "t.test", ref.group = "control") +
  stat_compare_means(method = "anova", label.y = 5)  

boxplot_observed_sign <- boxplot_observed +
  stat_compare_means(comparisons = my_comparisons, label = "p.signif", method = "t.test", ref.group = "control") +
  stat_compare_means(method = "anova", label.y = 250)  

boxplot_pielou_sign <- boxplot_pielou +
  stat_compare_means(method = "anova", label.y = 0.9)  

p_pielou <- plot_grid(boxplot_shannon_sign + 
                    theme(legend.position = "none", axis.text.x = element_text(angle = 45, hjust = 1)) +
                    labs(y = "Shannon diversity index", subtitle = "A"),
                  boxplot_observed_sign + 
                    theme(legend.position = "none", axis.text.x = element_text(angle = 45, hjust = 1)) +
                    labs(y = "Number of observed species", subtitle = "B"),
                  boxplot_pielou_sign +
                   theme(legend.position = "none", axis.text.x = element_text(angle = 45, hjust = 1)) +
                    labs(y = "Pielou's evenness", subtitle = "C"),
                  ncol=3, rel_widths = c(0.5, 0.5, 0.5), align="v", axis="lr") 
p_pielou
```
## Conclusion
case vs controls in 2-4 year olds shows similar alpha diversity signals as the 2-6 year old analysis. 

# B.2) Beta diversity

## Data prep
```{r}
ps_strat_young_RA <- ps_strat_young %>% to_RA()

otu_strat_young_RA <- otu_table(ps_strat_young_RA)[order(rowSums(otu_table(ps_strat_young_RA)), decreasing=T),] 

pcoa_strat_young <- pcoa_veg(t(otu_strat_young_RA), method = "bray")
```

## PCOA 

### 3 groups
```{r}
plot_pcoa_strat_young <- plot_pcoa(pcoa_strat_young, 
          ps_strat_young_RA, 
          group = subject_type, 
          name_legend = "subject type", cols = cols2$subject_type, n = 10, add_ellipse = T, add_species = F, format_single_cell = F, shape_point = 20, alpha_point = 0.8)$p 
plot_pcoa_strat_young
```

### 2 groups
```{r}
plot_pcoa(pcoa_strat_young, 
          ps_strat_young_RA, 
          group = environment, 
          name_legend = " environment", cols = cols2$environment, n = 10, add_ellipse = T, add_species = T, format_single_cell = F, shape_point = 20, alpha_point = 0.8)$p 
```

## PERMANOVA 
### 3 groups
```{r}
# Distance matrix
bc.strat.young <- vegdist(t(otu_strat_young_RA), method = "bray") 
bc.strat.young.m <- as.matrix(bc.strat.young)

bc.strat.young.m[1:5, 1:5]

# Testing
permutation <- 1000

set.seed(100)
perm.strat.young <- vegan::adonis2 (bc.strat.young ~ subject_type + Age_years, 
                             data = meta_strat_young, 
                             permutations = permutation, 
                             method = "bray", 
                             by = "margin")
perm.strat.young
```
### 2 groups

```{r}
set.seed(100)
permutation <- 1000

perm.case.contr <- vegan::adonis2 (bc.strat.young ~ environment + Age_years,
                            data = meta_strat_young,
                            permutations = permutation,
                            method = "bray", 
                            by = "margin")
perm.case.contr
```

### MC -IC

```{r}
ps_strat_young_RA_picu <- ps_strat_young %>% to_RA() %>% subset_samples(study == "PICU")

otu_strat_young_RA_picu <- otu_table(ps_strat_young_RA_picu)[order(rowSums(otu_table(ps_strat_young_RA)), decreasing=T),] 
meta_strat_young_picu <- ps_strat_young_RA_picu %>% meta_to_df()
bc.strat.young.picu <- vegdist(t(otu_strat_young_RA_picu), method = "bray") 

set.seed(100)
permutation <- 1000

perm.case.contr <- vegan::adonis2 (bc.strat.young.picu ~ Ward + Age_years,
                            data = meta_strat_young_picu,
                            permutations = permutation,
                            method = "bray", 
                            by = "margin")
perm.case.contr
```
## Conclusion
case vs controls in 2-4 year olds shows similar beta diversity signals as the 2-6 year old analysis. 

# B.3) Differential abundance
## Case - control analyses:
### Prepare data 29 ASVs of main MaAsLin2 analysis
```{r}
ps_with_samples_analyzed <- ps_RA %>% 
  subset_samples(Age < 10) %>% 
  pres_abund_filter(pres = 15.4, abund = 0.004) #NB: 15.4 samples corresponds to 5% of the samples included in one of the analyses. 

ASV_to_test <- ps_with_samples_analyzed %>% 
  otu_tab_to_df() %>% 
  column_to_rownames("#SampleID") %>% 
  colnames()

ps_strat_young_DA <- prune_taxa(ASV_to_test, ps_strat_young_RA)
otu_test_strat_young <- otu_tab_to_df(ps_strat_young_DA) %>% column_to_rownames("#SampleID") 

meta_strat_young <- ps_strat_young_DA %>% 
  meta_to_df() %>% 
  column_to_rownames("#SampleID") %>% 
  mutate(environment = fct_relevel(environment, c("control", "case")))

meta_strat_young %>% count(environment)
```

### Run Maaslin2
```{r}
DA_strat_young <- Maaslin2(
    input_data = otu_test_strat_young, 
    input_metadata = meta_strat_young,
    min_abundance = 0,
    min_prevalence = 0,
    normalization = "NONE" ,
    transform = "LOG",
    analysis_method = "LM",
    output = "demo_output",
    fixed_effects = c("environment", "Age_years"))

DA_strat_young <- as.data.frame(DA_strat_young$results)
```

### adjust df
```{r}
df_strat_young <- DA_strat_young %>% 
  filter(metadata == "environment") %>% 
  mutate(associated_wt = if_else(coef > 0, "case", "control"),
         significant_yn = if_else (qval <0.25, "yes", "no"),
         sign_association_with = if_else (qval <0.25, if_else(coef >0, "case", "control"), "not significant")) %>% 
  transform(., ass_and_sign = paste(associated_wt, significant_yn)) %>%
  mutate(ass_and_sign = fct_recode(ass_and_sign,
                                   "Case, ns" = "case no",
                                   "Case, significant" = "case yes",
                                   "Control, ns" = "control no",
                                   "Control, significant" = "control yes")) %>% 
  select(-c(sign_association_with))
```

### Create barplot
```{r}
ASVs <- df_strat_young %>%
  arrange(desc(coef), qval) %>%
  pull(feature) 
  
plot_adj <- df_strat_young %>%
  arrange(desc(coef), qval) %>% 
  mutate(OTU = format_OTU(ASVs) %>% fct_inorder(.) %>% fct_rev) 

plot_strat_young <- plot_adj %>%
    ggplot(aes(x = coef, y = OTU, label = OTU, fill = ass_and_sign)) +
      geom_col(color="grey10",  width = 0.7, position = "dodge") +
      scale_fill_manual(values = c("#fff2b2","gold", "#cfe1b9", "#66A61E" )) + # when you take pval use cols$comparison_2, when you only take control-MC/control-IC use comparison_3
  geom_richtext(data = plot_adj[plot_adj$coef < 0,], aes(x = 0, hjust = 0), nudge_x = 0.1, nudge_y = -0.05, size = 3.1, color = "grey30", fill = NA, label.color = NA) +
      geom_richtext(data = plot_adj[plot_adj$coef>0,], aes(x = 0, hjust = 1), nudge_x = -0.1, nudge_y = -0.05, size = 3.1, color = "grey30", fill = NA, label.color = NA) +
    theme(axis.text.y = element_blank(), axis.ticks.y = element_blank(), panel.border = element_blank(), panel.grid.major.y = element_blank(), legend.position = "top", axis.line.x = element_line(size = 0.1, linetype = "solid", colour = "grey10")) +
    labs(x = "model coefficient value (effect size)", y = "", fill = "") +
  annotate("text", x = -1.25, y= "*Actinomyces* (54)" , label = "Associated with: \nControls", fontface = "italic") +
  annotate("text", x = 2.25, y= "*Actinomyces* (54)" , label = "Associated with: \nCases", fontface = "italic")
plot_strat_young
```

## MC - IC analyses
### Prepare data 29 ASVs of main MaAsLin2 analysis
```{r}
ps_with_samples_analyzed <- ps_RA %>% 
  subset_samples(Age < 10) %>% 
  pres_abund_filter(pres = 15.4, abund = 0.004) #NB: 15.4 samples corresponds to 5% of the samples included in one of the analyses. 

ASV_to_test <- ps_with_samples_analyzed %>% 
  otu_tab_to_df() %>% 
  column_to_rownames("#SampleID") %>% 
  colnames()

ps_strat_young_DA_picu <- prune_taxa(ASV_to_test, ps_strat_young_RA_picu)
otu_test_strat_young_picu <- otu_tab_to_df(ps_strat_young_DA_picu) %>% column_to_rownames("#SampleID") 

meta_strat_young_picu <- ps_strat_young_DA_picu %>% 
  meta_to_df() %>% 
  column_to_rownames("#SampleID") %>% 
  mutate(environment = fct_relevel(environment, c("control", "case")))
```

### Run Maaslin2
```{r}
DA_strat_young_picu <- Maaslin2(
    input_data = otu_test_strat_young_picu, 
    input_metadata = meta_strat_young_picu,
    min_abundance = 0,
    min_prevalence = 0,
    normalization = "NONE" ,
    transform = "LOG",
    analysis_method = "LM",
    output = "demo_output",
    fixed_effects = c("Ward", "Age_years"))

DA_strat_young_picu_res <- as.data.frame(DA_strat_young_picu$results)
```

### adjust df
```{r}
df_strat_young_picu <- DA_strat_young_picu_res %>% 
  filter(metadata == "Ward") %>% 
  mutate(associated_wt = if_else(coef > 0, "ICU", "MCU"),
         significant_yn = if_else (qval <0.25, "yes", "no"),
         sign_association_with = if_else (qval <0.25, if_else(coef >0, "ICU", "MCU"), "not significant")) %>% 
  transform(., ass_and_sign = paste(associated_wt, significant_yn)) %>%
  mutate(ass_and_sign = fct_recode(ass_and_sign,
                                   "ICU, ns" = "ICU no",
                                   "ICU, significant" = "ICU yes",
                                   "MCU, ns" = "MCU no",
                                   "MCU, significant" = "MCU yes")) %>% 
  select(-c(sign_association_with))
```

### Create barplot
```{r}
ASVs <- df_strat_young_picu %>%
  arrange(desc(coef), qval) %>%
  pull(feature) 
  
plot_adj <- df_strat_young_picu %>%
  arrange(desc(coef), qval) %>% 
  mutate(OTU = format_OTU(ASVs) %>% fct_inorder(.) %>% fct_rev) 

plot_strat_young <- plot_adj %>%
    ggplot(aes(x = coef, y = OTU, label = OTU, fill = ass_and_sign)) +
      geom_col(color="grey10",  width = 0.7, position = "dodge") +
      scale_fill_manual(values = c("#fff2b2", "#cfe1b9" )) + # when you take pval use cols$comparison_2, when you only take control-MC/control-IC use comparison_3
  geom_richtext(data = plot_adj[plot_adj$coef < 0,], aes(x = 0, hjust = 0), nudge_x = 0.1, nudge_y = -0.05, size = 3.1, color = "grey30", fill = NA, label.color = NA) +
      geom_richtext(data = plot_adj[plot_adj$coef>0,], aes(x = 0, hjust = 1), nudge_x = -0.1, nudge_y = -0.05, size = 3.1, color = "grey30", fill = NA, label.color = NA) +
    theme(axis.text.y = element_blank(), axis.ticks.y = element_blank(), panel.border = element_blank(), panel.grid.major.y = element_blank(), legend.position = "top", axis.line.x = element_line(size = 0.1, linetype = "solid", colour = "grey10")) +
    labs(x = "model coefficient value (effect size)", y = "", fill = "") +
  annotate("text", x = -1.25, y= "*Neisseria* (19)" , label = "Associated with: \nMCU", fontface = "italic") +
  annotate("text", x = 2.25, y= "*Neisseria* (19)" , label = "Associated with: \nICU", fontface = "italic")
plot_strat_young
```

## Conclusion
Similar trends with the main analysis. 

#.
# C) 4-6 year old cases vs 4-6 year old controls

## Prepare data
```{r}
ps_strat_old <- ps %>% 
  subset_samples(age_cat == "> 4 years" & Age_years <7) %>% 
  pres_abund_filter(pres = 1, abund = 0)

meta_strat_old <- ps_strat_old %>% meta_to_df()

ps_strat_old_case <- ps %>% 
  subset_samples(age_cat == "> 4 years" & study == "PICU" & Age < 10) %>% 
  pres_abund_filter(pres = 1, abund = 0)

meta_strat_old_case <- ps_strat_old_case %>% 
  meta_to_df()
```

# C.1) Alpha diversity
### Calculate measures
```{r}
meta_strat_old$shannon <- estimate_richness(ps_strat_old, measure = "Shannon")
meta_strat_old$shannon <- unlist(meta_strat_old$shannon)
meta_strat_old$observed <- estimate_richness(ps_strat_old, measure ="Observed")
meta_strat_old$observed <- unlist(meta_strat_old$observed)

meta_strat_old %<>% 
  mutate(pielou = shannon/log(observed)) %>%
  column_to_rownames("#SampleID")

#assign new metadata with alpha div. measures to phyloseq
sample_data(ps_strat_old) <- meta_strat_old
```

### Visualization 
#### 3 groups
```{r}
boxplot_shannon <- meta_strat_old %>%
  ggplot(aes(x=subject_type, y=shannon, color = subject_type, fill =subject_type)) +
  geom_jitter(alpha = 1, width = 0.2) +
  geom_boxplot(alpha = 0.5, outlier.colour = NA) +
  scale_colour_manual(values = cols2$subject_type) +
  scale_fill_manual(values = cols2$subject_type) +
  labs(fill = "Study group", color = "Study group") +
  theme(axis.title.x = element_blank())

boxplot_shannon + stat_compare_means(method = "anova")

boxplot_observed <- meta_strat_old %>%
  ggplot(aes(x=subject_type, y=observed, color = subject_type, fill = subject_type)) +
  geom_jitter(alpha = 1, width = 0.2) +
  geom_boxplot(alpha = 0.5, outlier.colour = NA) +
  scale_colour_manual(values = cols2$subject_type) +
  scale_fill_manual(values = cols2$subject_type) +
  labs(fill = "Study group", color = "Study group") +
  theme(axis.title.x = element_blank())

boxplot_observed + stat_compare_means(method = "kruskal")

boxplot_pielou <- meta_strat_old %>%
  ggplot(aes(x=subject_type, y=pielou, color = subject_type, fill = subject_type)) +
  geom_jitter(alpha = 1, width = 0.2) +
  geom_boxplot(alpha = 0.5, outlier.colour = NA) +
  scale_colour_manual(values = cols2$subject_type) +
  scale_fill_manual(values = cols2$subject_type) +
  labs(fill = "Study group", color = "Study group") +
  theme(axis.title.x = element_blank())

boxplot_pielou + stat_compare_means(method = "anova")

my_comparisons <- list( c("control", "case (MC)"), c("control", "case (ICU)"))

boxplot_shannon_sign <- boxplot_shannon +
    stat_compare_means(comparisons = my_comparisons, label = "p.signif", method = "t.test", ref.group = "control") +
  stat_compare_means(method = "anova", label.y = 5)  

boxplot_observed_sign <- boxplot_observed +
  stat_compare_means(comparisons = my_comparisons, label = "p.signif", method = "t.test", ref.group = "control") +
  stat_compare_means(method = "anova", label.y = 250)  

boxplot_pielou_sign <- boxplot_pielou +
  stat_compare_means(method = "anova", label.y = 0.9)  

p_pielou <- plot_grid(boxplot_shannon_sign + 
                    theme(legend.position = "none", axis.text.x = element_text(angle = 45, hjust = 1)) +
                    labs(y = "Shannon diversity index", subtitle = "A"),
                  boxplot_observed_sign + 
                    theme(legend.position = "none", axis.text.x = element_text(angle = 45, hjust = 1)) +
                    labs(y = "Number of observed species", subtitle = "B"),
                  boxplot_pielou_sign +
                   theme(legend.position = "none", axis.text.x = element_text(angle = 45, hjust = 1)) +
                    labs(y = "Pielou's evenness", subtitle = "C"),
                  ncol=3, rel_widths = c(0.5, 0.5, 0.5), align="v", axis="lr") 
p_pielou
```

#### 2 groups
```{r}
boxplot_shannon_case_contr <- meta_strat_old %>%
  ggplot(aes(x=environment, y=shannon, color = environment, fill =environment)) +
  geom_jitter(alpha = 1, width = 0.2) +
  geom_boxplot(alpha = 0.5, outlier.colour = NA) +
  scale_colour_manual(values = cols2$environment) +
  scale_fill_manual(values =cols2$environment) +
  labs(subtitle = "Shannon diversity index in controls vs cases")

boxplot_shannon_case_contr + stat_compare_means(method ="anova")

boxplot_obs_case_contr <- meta_strat_old %>%
  ggplot(aes(x=environment, y=observed, color = environment, fill = environment)) +
  geom_jitter(alpha = 1, width = 0.2) +
  geom_boxplot(alpha = 0.5, outlier.colour = NA) +
  scale_colour_manual(values = cols2$environment) +
  scale_fill_manual(values =cols2$environment) +
  labs(subtitle="Richness in controls vs cases")

boxplot_obs_case_contr + stat_compare_means(method ="anova")

boxplot_pielou_case_contr <- meta_strat_old %>%
  ggplot(aes(x=environment, y=pielou, color = environment, fill = environment)) +
  geom_jitter(alpha = 1, width = 0.2) +
  geom_boxplot(alpha = 0.5, outlier.colour = NA) +
  scale_colour_manual(values = cols2$environment) +
  scale_fill_manual(values =cols2$environment) +
  labs(subtitle="Evenness in controls vs cases")

boxplot_pielou_case_contr + stat_compare_means(method ="anova")
```

#### MC-IC
#####  Calculate measures
```{r}
meta_strat_old_case$shannon <- estimate_richness(ps_strat_old_case, measure = "Shannon")
meta_strat_old_case$shannon <- unlist(meta_strat_old_case$shannon)
meta_strat_old_case$observed <- estimate_richness(ps_strat_old_case, measure ="Observed")
meta_strat_old_case$observed <- unlist(meta_strat_old_case$observed)

meta_strat_old_case %<>% 
  mutate(pielou = shannon/log(observed)) %>%
  column_to_rownames("#SampleID")

#assign new metadata with alpha div. measures to phyloseq
sample_data(ps_strat_old_case) <- meta_strat_old_case
```

##### Visualize
```{r}
boxplot_shannon <- meta_strat_old_case %>%
  filter(study == "PICU") %>% 
  ggplot(aes(x=Ward, y=shannon, color = Ward, fill =Ward)) +
  geom_jitter(alpha = 1, width = 0.2) +
  geom_boxplot(alpha = 0.5, outlier.colour = NA) +
  scale_colour_manual(values = cols2$ward_3) +
  scale_fill_manual(values = cols2$ward_3) +
  labs(fill = "Ward", color = "Ward") +
  theme(axis.title.x = element_blank())

boxplot_shannon + stat_compare_means(method = "anova")

boxplot_observed <- meta_strat_old_case %>%
  filter(study == "PICU") %>% 
  ggplot(aes(x=Ward, y=observed, color = Ward, fill =Ward)) +
  geom_jitter(alpha = 1, width = 0.2) +
  geom_boxplot(alpha = 0.5, outlier.colour = NA) +
  scale_colour_manual(values = cols2$ward_3) +
  scale_fill_manual(values = cols2$ward_3) +
  labs(fill = "Ward", color = "Ward") +
  theme(axis.title.x = element_blank())

boxplot_observed + stat_compare_means(method = "kruskal")

boxplot_pielou <- meta_strat_old_case %>%
  filter(study == "PICU") %>% 
  ggplot(aes(x=Ward, y=pielou, color = Ward, fill =Ward)) +
  geom_jitter(alpha = 1, width = 0.2) +
  geom_boxplot(alpha = 0.5, outlier.colour = NA) +
  scale_colour_manual(values = cols2$ward_3) +
  scale_fill_manual(values = cols2$ward_3) +
  labs(fill = "Ward", color = "Ward") +
  theme(axis.title.x = element_blank())

boxplot_pielou + stat_compare_means(method = "anova")

my_comparisons <- list( c("control", "case (MC)"), c("control", "case (ICU)"))

boxplot_shannon_sign <- boxplot_shannon +
    stat_compare_means(comparisons = my_comparisons, label = "p.signif", method = "t.test", ref.group = "control") +
  stat_compare_means(method = "anova", label.y = 5)  

boxplot_observed_sign <- boxplot_observed +
  stat_compare_means(comparisons = my_comparisons, label = "p.signif", method = "t.test", ref.group = "control") +
  stat_compare_means(method = "anova", label.y = 250)  

boxplot_pielou_sign <- boxplot_pielou +
  stat_compare_means(method = "anova", label.y = 0.9)  

p_pielou <- plot_grid(boxplot_shannon_sign + 
                    theme(legend.position = "none", axis.text.x = element_text(angle = 45, hjust = 1)) +
                    labs(y = "Shannon diversity index", subtitle = "A"),
                  boxplot_observed_sign + 
                    theme(legend.position = "none", axis.text.x = element_text(angle = 45, hjust = 1)) +
                    labs(y = "Number of observed species", subtitle = "B"),
                  boxplot_pielou_sign +
                   theme(legend.position = "none", axis.text.x = element_text(angle = 45, hjust = 1)) +
                    labs(y = "Pielou's evenness", subtitle = "C"),
                  ncol=3, rel_widths = c(0.5, 0.5, 0.5), align="v", axis="lr") 
p_pielou
```

## Conclusion
case vs controls in >4 year olds shows similar alpha diversity signals as the 2-6 year old analysis.

# C.2) Beta diversity

## Data prep
```{r}
ps_strat_old_RA <- ps_strat_old %>% to_RA()

otu_strat_old_RA <- otu_table(ps_strat_old_RA)[order(rowSums(otu_table(ps_strat_old_RA)), decreasing=T),] 

pcoa_strat_old <- pcoa_veg(t(otu_strat_old_RA), method = "bray")

# within cases
ps_strat_old_RA_case <- ps_strat_old_case %>% to_RA()

otu_strat_old_RA_case <- otu_table(ps_strat_old_RA_case)[order(rowSums(otu_table(ps_strat_old_RA_case)), decreasing=T),] 

pcoa_strat_old_case <- pcoa_veg(t(otu_strat_old_RA_case), method = "bray")
```

## PCOA 

### 3 groups
```{r}
plot_pcoa_strat_old <- plot_pcoa(pcoa_strat_old, 
          ps_strat_old_RA, 
          group = subject_type, 
          name_legend = "subject type", cols = cols2$subject_type, n = 10, add_ellipse = T, add_species = F, format_single_cell = F, shape_point = 20, alpha_point = 0.8)$p 
plot_pcoa_strat_old
```

### 2 groups
```{r}
plot_pcoa(pcoa_strat_old, 
          ps_strat_old_RA, 
          group = environment, 
          name_legend = " environment", cols = cols2$environment, n = 10, add_ellipse = T, add_species = T, format_single_cell = F, shape_point = 20, alpha_point = 0.8)$p 
```

### MC -IC
```{r}
plot_pcoa(pcoa_strat_old_case, 
          ps_strat_old_RA_case, 
          group = Ward, 
          name_legend = "Ward", cols = cols2$ward_3, n = 10, add_ellipse = T, add_species = T, format_single_cell = F, shape_point = 20, alpha_point = 0.8)$p 
```

## PERMANOVA 
### 3 groups
```{r}
# Distance matrix
bc.strat.old <- vegdist(t(otu_strat_old_RA), method = "bray") 
bc.strat.old.m <- as.matrix(bc.strat.old)

bc.strat.old.m[1:5, 1:5]

# Testing
permutation <- 1000

set.seed(100)
perm.strat.old <- vegan::adonis2 (bc.strat.old ~ subject_type + Age_years, 
                             data = meta_strat_old, 
                             permutations = permutation, 
                             method = "bray",
                             by = "margin")
perm.strat.old
```
### 2 groups

```{r}
set.seed(100)
permutation <- 1000

perm.case.contr <- vegan::adonis2 (bc.strat.old ~ environment + Age_years,
                            data = meta_strat_old,
                            permutations = permutation,
                            method = "bray",
                            by = "margin")
perm.case.contr
```

### MC -IC

```{r}
bc.strat.old.picu <- vegdist(t(otu_strat_old_RA_case), method = "bray") 

set.seed(100)
permutation <- 1000

perm.case.contr <- vegan::adonis2 (bc.strat.old.picu ~ Ward + Age_years,
                            data = meta_strat_old_case,
                            permutations = permutation,
                            method = "bray", 
                            by = "margin")
perm.case.contr
```

## Conclusion
case vs controls in > 4 year olds shows similar beta diversity signals as the 2-6 year old analysis. 

# C.3) Differential abundance

## Case - control analyses: 
###Prepare data 29 ASVs of main MaAsLin2 analysis
```{r}
ps_with_samples_analyzed <- ps_RA %>% 
  subset_samples(Age < 10) %>% 
  pres_abund_filter(pres = 15.4, abund = 0.004) #NB: 15.4 samples corresponds to 5% of the samples included in one of the analyses. 

ASV_to_test <- ps_with_samples_analyzed %>% 
  otu_tab_to_df() %>% 
  column_to_rownames("#SampleID") %>% 
  colnames()

ps_strat_old_DA <- prune_taxa(ASV_to_test, ps_strat_old_RA)
otu_test_strat_old <- otu_tab_to_df(ps_strat_old_DA) %>% column_to_rownames("#SampleID") 

meta_strat_old <- ps_strat_old_DA %>% 
  meta_to_df() %>% 
  column_to_rownames("#SampleID") %>% 
  mutate(environment = fct_relevel(environment, c("control", "case")))
```

### Run Maaslin2
```{r}
DA_strat_old <- Maaslin2(
    input_data = otu_test_strat_old, 
    input_metadata = meta_strat_old,
    min_abundance = 0,
    min_prevalence = 0,
    normalization = "NONE" ,
    transform = "LOG",
    analysis_method = "LM",
    output = "demo_output",
    fixed_effects = c("environment", "Age_years"))

DA_strat_old <- as.data.frame(DA_strat_old$results)
```

### adjust df
```{r}
df_strat_old <- DA_strat_old %>% 
  filter(metadata == "environment") %>% 
  mutate(associated_wt = if_else(coef > 0, "case", "control"),
         significant_yn = if_else (qval <0.25, "yes", "no"),
         sign_association_with = if_else (qval <0.25, if_else(coef >0, "case", "control"), "not significant")) %>% 
  transform(., ass_and_sign = paste(associated_wt, significant_yn)) %>%
  mutate(ass_and_sign = fct_recode(ass_and_sign,
                                   "Case, ns" = "case no",
                                   "Case, significant" = "case yes",
                                   "Control, ns" = "control no",
                                   "Control, significant" = "control yes")) %>% 
  select(-c(sign_association_with))
```

### Create barplot
```{r}
ASVs <- df_strat_old %>%
  arrange(desc(coef), qval) %>%
  pull(feature) 
  
plot_adj <- df_strat_old %>%
  arrange(desc(coef), qval) %>% 
  mutate(OTU = format_OTU(ASVs) %>% fct_inorder(.) %>% fct_rev) 

plot_strat_old <- plot_adj %>%
    ggplot(aes(x = coef, y = OTU, label = OTU, fill = ass_and_sign)) +
      geom_col(color="grey10",  width = 0.7, position = "dodge") +
      scale_fill_manual(values = c("#fff2b2","gold", "#cfe1b9", "#66A61E" )) + # when you take pval use cols$comparison_2, when you only take control-MC/control-IC use comparison_3
  geom_richtext(data = plot_adj[plot_adj$coef < 0,], aes(x = 0, hjust = 0), nudge_x = 0.1, nudge_y = -0.05, size = 3.1, color = "grey30", fill = NA, label.color = NA) +
      geom_richtext(data = plot_adj[plot_adj$coef>0,], aes(x = 0, hjust = 1), nudge_x = -0.1, nudge_y = -0.05, size = 3.1, color = "grey30", fill = NA, label.color = NA) +
    theme(axis.text.y = element_blank(), axis.ticks.y = element_blank(), panel.border = element_blank(), panel.grid.major.y = element_blank(), legend.position = "top", axis.line.x = element_line(size = 0.1, linetype = "solid", colour = "grey10")) +
    labs(x = "model coefficient value (effect size)", y = "", fill = "") +
  annotate("text", x = -1.25, y= "*Actinomyces* (54)" , label = "Associated with: \nControls", fontface = "italic") +
  annotate("text", x = 2.25, y= "*Actinomyces* (54)" , label = "Associated with: \nCases", fontface = "italic")
plot_strat_old
```
## MC - IC analyses
### Prepare data 29 ASVs of main MaAsLin2 analysis
```{r}
ps_with_samples_analyzed <- ps_RA %>% 
  subset_samples(Age < 10) %>% 
  pres_abund_filter(pres = 15.4, abund = 0.004) #NB: 15.4 samples corresponds to 5% of the samples included in one of the analyses. 

ASV_to_test <- ps_with_samples_analyzed %>% 
  otu_tab_to_df() %>% 
  column_to_rownames("#SampleID") %>% 
  colnames()

ps_strat_old_DA_picu <- prune_taxa(ASV_to_test, ps_strat_old_RA_case)
otu_test_strat_old_picu <- otu_tab_to_df(ps_strat_old_DA_picu) %>% column_to_rownames("#SampleID") 

meta_strat_young_picu <- ps_strat_old_DA_picu %>% 
  meta_to_df() %>% 
  column_to_rownames("#SampleID") %>% 
  mutate(environment = fct_relevel(environment, c("control", "case")))
```

### Run Maaslin2
```{r}
DA_strat_old_picu <- Maaslin2(
    input_data = otu_test_strat_old_picu, 
    input_metadata = meta_strat_young_picu,
    min_abundance = 0,
    min_prevalence = 0,
    normalization = "NONE" ,
    transform = "LOG",
    analysis_method = "LM",
    output = "demo_output",
    fixed_effects = c("Ward", "Age_years"))

DA_strat_old_picu_res <- as.data.frame(DA_strat_old_picu$results)
```

### adjust df
```{r}
df_strat_old_picu <- DA_strat_old_picu_res %>% 
  filter(metadata == "Ward") %>% 
  mutate(associated_wt = if_else(coef > 0, "ICU", "MCU"),
         significant_yn = if_else (qval <0.25, "yes", "no"),
         sign_association_with = if_else (qval <0.25, if_else(coef >0, "ICU", "MCU"), "not significant")) %>% 
  transform(., ass_and_sign = paste(associated_wt, significant_yn)) %>%
  mutate(ass_and_sign = fct_recode(ass_and_sign,
                                   "ICU, ns" = "ICU no",
                                   "ICU, significant" = "ICU yes",
                                   "MCU, ns" = "MCU no",
                                   "MCU, significant" = "MCU yes")) %>% 
  select(-c(sign_association_with))
```

### Create barplot
```{r}
ASVs <- df_strat_old_picu %>%
  arrange(desc(coef), qval) %>%
  pull(feature) 
  
plot_adj <- df_strat_old_picu %>%
  arrange(desc(coef), qval) %>% 
  mutate(OTU = format_OTU(ASVs) %>% fct_inorder(.) %>% fct_rev) 

plot_strat_old <- plot_adj %>%
    ggplot(aes(x = coef, y = OTU, label = OTU, fill = ass_and_sign)) +
      geom_col(color="grey10",  width = 0.7, position = "dodge") +
      scale_fill_manual(values = c("#fff2b2", "#cfe1b9" )) + # when you take pval use cols$comparison_2, when you only take control-MC/control-IC use comparison_3
  geom_richtext(data = plot_adj[plot_adj$coef < 0,], aes(x = 0, hjust = 0), nudge_x = 0.1, nudge_y = -0.05, size = 3.1, color = "grey30", fill = NA, label.color = NA) +
      geom_richtext(data = plot_adj[plot_adj$coef>0,], aes(x = 0, hjust = 1), nudge_x = -0.1, nudge_y = -0.05, size = 3.1, color = "grey30", fill = NA, label.color = NA) +
    theme(axis.text.y = element_blank(), axis.ticks.y = element_blank(), panel.border = element_blank(), panel.grid.major.y = element_blank(), legend.position = "top", axis.line.x = element_line(size = 0.1, linetype = "solid", colour = "grey10")) +
    labs(x = "model coefficient value (effect size)", y = "", fill = "") +
  annotate("text", x = -1.25, y= "*Neisseria* (19)" , label = "Associated with: \nMCU", fontface = "italic") +
  annotate("text", x = 2.25, y= "*Neisseria* (19)" , label = "Associated with: \nICU", fontface = "italic")
plot_strat_old
```

## Conclusion
Similar trends with the main analysis. 

#.
# D) MCU vs ICU?