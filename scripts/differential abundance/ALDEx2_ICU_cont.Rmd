---
title: "Untitled"
author: "Fien van Beveren"
date: "2023-11-02"
output: html_document
---

# Load packages

```{r}
library(tidyverse); library(magrittr); library(here); library(glue); 
library(phyloseq); library(decontam); library(ggforce); library(ALDEx2);
library(microbiomer); library(Maaslin2); library(RColorBrewer); library(patchwork); 
library(ggpubr); library(vegan); library(ggtext); library(writexl)

subdir_name <- "ALDEx2_ICU_cont"

# set paths
knitr::opts_knit$set(root.dir=".", aliases=c(h = "fig.height", w = "fig.width", ow = "out.width"))
knitr::opts_chunk$set(dev=c('png', 'pdf'), 
                      fig.path=here("results", "figures", glue("{subdir_name}/")), dpi=300)
theme_set(theme_light()) 
```

# Load functions

```{r}
source(here("src", "utils.R")) # please see the src-folder for some custom functions used in this script.
source(here("src", "load_dada.R"))
source(here("src", "colour_scheme.R"))
source(here("src", "colour_scheme_2.R"))
source(here::here("src", "calc_ellipses.R"))
source(here::here("src", "pcoa_veg.R"))
source(here::here("src", "plot_pcoa.R"))
```

# Load data
```{r}
ps <- readRDS(here::here("data", "processed", "ps_comb.Rds"))
ps_RA <- ps %>% to_RA() 
```

# Objectives
* perform ALDEx2 differential abundance analysis on the 29ASVs tested with MaAsLin2
* see:https://www.bioconductor.org/packages/devel/bioc/vignettes/ALDEx2/inst/doc/ALDEx2_vignette.html#1_Introduction_to_ALDEx2
* Of note: not RA data because ALDEx2 uses CLR normalization and not TSS. So don't transform to RA. 

# 29 ASVs to test
```{r}
ps_samples_analyzed <- ps %>% subset_samples(Age < 10)

ps_test <- ps_samples_analyzed %>% 
    pres_abund_filter(pres = 15.4, abund = 0.004) %>% #NB: 15.4 samples corresponds to 5% of the samples included in one of the analyses. 
    subset_samples(Age <7 & !subject_type == "case (MC)")

ps_test %>% meta_to_df() %>% dplyr::count(subject_type)

otu_test <- otu_tab_to_df(ps_test) %>% column_to_rownames("#SampleID") %>% t
meta_test <- ps_test %>% meta_to_df() %>% dplyr::select(`#SampleID`, subject_type, Age_years)
```

# ALDEx2 - following chapter 11 of tutorial 
NB: not corrected for age
NB2: output is wilcoxon rank test. 
```{r}
set.seed(100)
x <- aldex.clr(otu_test, meta_test$subject_type, mc.samples = 126, denom = "all", verbose = FALSE)

# calculates expected values of the Welch's t-test and Wilcoxon rank
# test on the data returned by aldex.clr
x_tt <- aldex.ttest(x, paired.test = FALSE, verbose = FALSE)

# Determines the median clr abundance of the feature in all samples and in
# groups, the median difference between the two groups, the median variation
# within each group and the effect size, which is the median of the ratio
# of the between group difference and the larger of the variance within groups
x_effect <- aldex.effect(x, CI = TRUE, verbose = FALSE)

# combine all outputs 
aldex_out <- data.frame(x_tt, x_effect)

# plot 
par(mfrow = c(1, 2))
aldex.plot(aldex_out,
           type = "MA",
           test = "welch",
           xlab = "Log-ratio abundance",
           ylab = "Difference",
           cutoff = 0.05)

aldex.plot(aldex_out,
           type = "MW",
           test = "welch",
           xlab = "Dispersion",
           ylab = "Difference",
           cutoff = 0.05)

aldex_out %>%
  rownames_to_column(var = "Genus") %>%
  # here we choose the wilcoxon output rather than t-test output
  filter(wi.eBH <= 0.05)  %>%
    dplyr::rename(pval_ttest = we.ep,
           qval_ttest = we.eBH,
           pval_wilcox = wi.ep,
           qval_wilcox = wi.eBH) %>%
  dplyr::select(Genus, pval_wilcox, qval_wilcox, effect, overlap) 
```

# ALDEx2 - glm not correct for age
NB: check of verschil met wilcox en hoe groot dit verschil is. 
```{r}
set.seed(100)
mm <- model.matrix(~ subject_type, meta_test)
aldex_clr_glm <- aldex.clr(otu_test, mm, mc.samples = 126, denom = "all", verbose = FALSE)

# calculate expected values of glm model...
glm.test <- aldex.glm(aldex_clr_glm, mm)
glm.test %>% colnames()

glm.test %>% 
    rownames_to_column(var = "ASV") %>%
    filter(`model.subject_type.L Pr(>|t|).BH` <= 0.05) %>% 
    dplyr::select(ASV, `model.subject_type.L Pr(>|t|).BH`, `model.subject_type.L Pr(>|t|)`)
```

# ALDEx2 - glm correct for age 
Predictor: case/control status and age. 
Outcome: CLR of the ASV
```{r}
set.seed(100)
#model that glm will use 
mm_age <- model.matrix(~ subject_type + Age_years, meta_test)
aldex_clr_glm_age <- aldex.clr(otu_test, mm_age, mc.samples = 126, denom = "all", verbose = FALSE)

# calculate expected values of glm model...
glm.test.age <- aldex.glm(aldex_clr_glm_age, mm_age)

glm.test.age %>% 
    rownames_to_column(var = "ASV") %>%
    filter(`model.subject_type.L Pr(>|t|).BH` <= 0.05) %>% 
    dplyr::select(ASV, `model.subject_type.L Pr(>|t|).BH`, `model.subject_type.L Pr(>|t|)`)

# Determines the median clr abundance of the feature in all samples and in
# groups, the median difference between the two groups, the median variation
# within each group and the effect size, which is the median of the ratio
# of the between group difference and the larger of the variance within groups
glm.eff.age <- aldex.glm.effect(aldex_clr_glm_age, CI = T)

# combine all outputs 
aldex_out_glm_age <- data.frame(glm.test.age, glm.eff.age)

# plot data
feature <- aldex_out_glm_age %>% rownames()
aldex_out_glm_age%>% colnames()
df_plot_age <- aldex_out_glm_age %>% 
    dplyr::rename(intercept_est = X.Intercept..Estimate,
           interecept_se = X.Intercept..Std..Error,
           intercept_tstat = X.Intercept..t.value,
           intercept_pval = X.Intercept..Pr...t..,
           subj_est = model.subject_type.L.Estimate,
           subj_se = model.subject_type.L.Std..Error,
           subj_tstat = model.subject_type.L.t.value,
           subj_pval = model.subject_type.L.Pr...t..,
           age_est = model.Age_years.Estimate,
           age_se = model.Age_years.Std..Error,
           age_tstat = model.Age_years.t.value,
           age_pval = model.Age_years.Pr...t..,
           intercept_qval = X.Intercept..Pr...t...BH,
           subj_qval = model.subject_type.L.Pr...t...BH,
           age_qval = model.Age_years.Pr...t...BH,
           rab_all = subject_type.L.rab.all,
           rab_win_control = subject_type.L.rab.win..0.707106781186547,
           rab_win_case = subject_type.L.rab.win.0.707106781186548,
           diff_btw = subject_type.L.diff.btw,
           diff_win = subject_type.L.diff.win,
           effect = subject_type.L.effect,
           overlap = subject_type.L.overlap) %>% 
    dplyr::select(starts_with("subj_"), rab_all, rab_win_control, rab_win_case, diff_btw, diff_win, effect, overlap) %>% 
    mutate(associated_wt = if_else(effect < 0, "control", "case (ICU)")) 

df_plot_age$feature  <- feature 

df_plot_age %>%
  rownames_to_column(var = "Genus") %>%
  # here we choose the wilcoxon output rather than t-test output
  dplyr::filter(subj_qval <= 0.05)  %>%
  dplyr::select(Genus, subj_pval, subj_qval, effect, overlap, associated_wt)

df_plot_age %>% 
    arrange(subj_qval) %>% 
    write_xlsx(., path = "aldeX_res_ICU_cont.xlsx")
```
### plot qval
```{r}
df_plot_age %>% 
    ggplot(aes(x = effect, y = -log10(subj_qval), fill = associated_wt)) +
    coord_cartesian(xlim =  c(-4, 4), ylim = c(0, 13), clip = "off") +
    annotate("rect", xmin = -1, xmax = 1, ymin = -Inf, ymax = -log10(0.1), alpha = .15, fill = "gray50") +
    annotate("rect", xmin = -1, xmax = -Inf, ymin = -log10(0.1), ymax = Inf, alpha = .5, fill = "#FFFF99") +
    annotate("rect", xmin = 1, xmax = Inf, ymin = -log10(0.1), ymax = Inf, alpha = .5, fill = "#7FC97F") +
    geom_point(shape = 21, color = "grey80") +
    scale_fill_manual(values = c( "goldenrod1", "forestgreen")) +
    theme(axis.title.x = element_markdown(), axis.title.y = element_markdown(), legend.text = element_markdown(),
          panel.grid.minor = element_blank()) +
    labs(x = "effect size", y = "_q_-value (-log<sub>10</sub>)") +
    geom_vline(xintercept = c(-1, 1), linetype = "dotted", color = "gray50") +
    geom_hline(yintercept = -log10(0.1), linetype = "dotted", color = "gray50") +
    scale_x_continuous(breaks = c(-4, -3,-2, -1, 0, 1, 2, 3,4)) + 
    scale_size_continuous(range = c(0.5, 4), guide = "none") +
    #scale_fill_manual(name = "Node type", values = c(`Neisseria_19` = "#236F21", `Streptococcus_7` = "#2CB11B", `Haemophilus_6` = "#9FCC6B",`ns` = "grey65")) +
    ggrepel::geom_label_repel(data = df_plot_age %>% 
                                filter(-log10(subj_qval) < -log10(0.1)), 
                              aes(label = feature), 
                              size = 3, fill = NA, label.size = NA, inherit.aes = T, parse = T, 
                              min.segment.length = unit(0, 'lines'), force_pull = 10, color = "grey50") + 
    #scale_color_manual(name = "Node type", values = c(`Neisseria_19` = "#236F21", `Streptococcus_7` = "#2CB11B",  `Haemophilus_6` = "#9FCC6B",`ns` = "grey65")) +
    annotate(geom = "richtext", x = -3.5, y = 12.75, label = glue("<span style='color:{cols$subject_type_2['cases']}'> cases</span>"),
             hjust = "left", size = 3, fill = NA, label.color = NA, fontface = "bold") +
    annotate(geom = "richtext", x = 3.5, y = 12.75, label = glue("<span style='color:{cols$subject_type_2['control']}'>control</span>"),
             hjust = "right", size = 3, fill = NA, label.color = NA, fontface = "bold") +
    annotate("segment", x = -3.5, xend = -4, y = 12.75, yend = 12.75,
             colour = "goldenrod1", size = 0.8, arrow = arrow(length = unit(0.20, "cm")), lineend = "butt", linejoin = "mitre") +
    annotate("segment", x = 3.5, xend = 4, y = 12.75, yend = 12.75,
             colour = "#7FC97F", size = 0.8, arrow = arrow(length = unit(0.20, "cm")), lineend = "butt", linejoin = "mitre") +
    annotate("curve", x = 4.2, xend = 4.8, y = (-log10(0.1) - 0.1), yend = (-log10(0.1) - 0.1),
             colour = "gray50", size = 0.5, lineend = "butt", curvature = 0.4) +
    annotate(geom = "richtext", x = 4.8, y = (-log10(0.1)), label = 
             "_q_-value = 0.1", color = "gray50", hjust = "left", size = 3, fill = NA, label.color = NA) 
```
