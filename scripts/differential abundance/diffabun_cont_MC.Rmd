---
title: "Untitled"
author: "Fien van Beveren"
date: "7/8/2022"
output: html_document
---
# Load packages

```{r}
library(tidyverse)
library(magrittr)
library(here)
library(glue)
library(phyloseq)
library(cowplot)
library(haven)
library(Maaslin2)
library(gdata)
library(writexl)
library(ggtext)
library(EnhancedVolcano)

subdir_name <- "diffabun_cont_MC"

# set paths
knitr::opts_knit$set(root.dir=".", aliases=c(h = "fig.height", w = "fig.width", ow = "out.width"))
knitr::opts_chunk$set(dev=c('png', 'pdf'), 
                      fig.path=here("results", "figures", glue("{subdir_name}/")), dpi=300)
theme_set(theme_light()) 
```

```{r knit, echo=F, eval=FALSE}
rmarkdown::render(input = here("scripts", str_c(subdir_name, ".Rmd")), output_dir = here("results"))
```

# Load functions

```{r}
source(here("src", "utils.R")) # please see the src-folder for some custom functions used in this script.
source(here("src", "load_dada.R"))
source(here("src", "colour_scheme.R"))
```

## Functions differential abundance
```{r}
otu_tab_to_df <- function(ps, sample_name = "#SampleID") {
  otu_tab <- phyloseq::otu_table(ps)
  df <- otu_tab %>%
    t %>%
    data.frame(check.names = F) %>%
    tibble::rownames_to_column(sample_name)
  return(df)
}
```

# Load data

```{r}
ps_comb <- readRDS(here("data", "processed", "ps_comb.Rds")) # PICU & MOL data 
ps_RA <- ps_comb %>% to_RA() 

ps_MC <- ps_RA %>% subset_samples(!subject_type == "case (ICU)") %>% subset_samples (Age_years <7)
meta <- ps_MC %>% meta_to_df()
```

# Objectives
Differential abundance: 
* Differential abundance testing on taxa to see if some taxa are more associated with control-MC samples. 
* 2 t/m 6 year olds (only control samples for this age range)
* What thresholds to use? 
  - Similar to MC-IC
  - Other possibilities? 
* Maaslin2: used the type of data. 
  - RA 
  - log transformation
  - no normalization
  - filtering; when inspecting the underlying code of the Maaslin2-function it seemed like the taxa data was first filtered and then normalized. Filtering was done on abundance and prevalence parameters. When normalizing abundance data with TSS you basically calculate RA measures. When your input is RA, then you filter data 'heavily'
* visualize with volcano or horizontal barplots in direction of effect
* If no results: Test on RA of taxa present in high abundance in the samples. Eg. top 15 ASVs. 

We used MaAsLin2-package to identify taxa that were more differentially abundant in the control or MC group. 
For different ways to analyse the differential abundance see: 
Microbiome differential abundance methods produce different results across 38 datasets by Nearing et al., 2022. "However, MaAsLin2 (particularly with rarefied data) could also be a reasonable choice for users looking for increased statistical power at the potential cost of more false positives."

How to use Maaslin2: 
- RA data as input
- Filter does not matter if you use RA input.
- If you do raw reads and then filter within the Maaslin2 function the RA changes because you first filter and than transform data to RA. Best order would be transform to RA. Than filter? 

#.
See excel sheet Maaslin2. Goal was to test 25-30 ASVs. 

We tested all these ASVs. Reasoning behind this was; rare ASVs that are related to disease/disease severity, even when they are present in low(er) abundance will not be tested this way. 
Moreover we want to test the "biomarkers" of the clusters. Therefore inspect if these are included in the ASVs tested. To get an idea of the biomarkers of a cluster, I will use the indicspecies package to see what ASVs are characteristic for each cluster. These ASVs will be tested as well with Maaslin. 

# Select list of ASVs I want to test 
I want to test ASVs that are present in 0.4%RA in 5% of the samples that go into one of the differential abundant analyses -> children <10 year both cases and controls. This is because it prevents input from older children in the selection of what ASVs to test. In the samples that you will eventuall compare the older samples are not included and if you would select ASVs to test bases on these samples from older children it will not be associated/representative. It will not make sense to test the ASV between samples that it's not found in. 

```{r}
ps_young <- ps_comb %>% subset_samples(Age < 10)
ps_young_RA <- ps_young %>% to_RA()
meta_young <- ps_young %>% meta_to_df() %>% column_to_rownames("#SampleID")
otu <- otu_tab_to_df(ps_young_RA) %>% column_to_rownames("#SampleID")

diff_ASV_to_test <-  Maaslin2(
    input_data = otu, 
    input_metadata = meta_young,
    min_abundance = 0.004,
    min_prevalence = 0.05,
    normalization = "NONE" ,
    transform = "LOG",
    analysis_method = "LM",
    output = "demo_output",
    fixed_effects = c("environment"))

diff_ASV_to_test_results <- as.data.frame(diff_ASV_to_test$results) 
ASV_to_test <- diff_ASV_to_test_results  %>% pull(feature) # step 1: list of ASVs to test
as.data.frame(diff_ASV_to_test$results) %>% count(qval < 0.25)
```

# create ps with control & MC samples 2-6 yrs
```{r}
ps_MC <- ps_young %>% subset_samples(!subject_type == "case (ICU)") %>% subset_samples(Age <7) # step 2: Exclude samples that do not go into the comparison
ps_MC_RA <- ps_MC %>% to_RA() #step 3: transform OTU table to RA
ps.test <- prune_taxa(ASV_to_test, ps_MC_RA) #step 4: remove taxa from otu table that are not in the list

otu_test <- otu_tab_to_df(ps.test) %>% column_to_rownames("#SampleID") # step 5: create OTU table
meta_test <- ps.test %>% meta_to_df() %>% column_to_rownames("#SampleID")
```

# test control-MC with Maaslin2
```{r}
diff_MC <-  Maaslin2(
    input_data = otu_test, 
    input_metadata = meta_test,
    min_abundance = 0,
    min_prevalence = 0,
    normalization = "NONE" ,
    transform = "LOG",
    analysis_method = "LM",
    output = "demo_output",
    fixed_effects = c("subject_type"))

diff_MC_results <- as.data.frame(diff_MC$results)
diff_MC_results
write_xlsx(diff_MC_results, "/Users/fienvanbeveren/Documents/picu/results/vs controls/vs MC/diff_MC_results.xlsx") 
```

# test control-MC corrected for age 
```{r}
diff_MC_age <-  Maaslin2(
    input_data = otu_test, 
    input_metadata = meta_test,
    min_abundance = 0,
    min_prevalence = 0,
    normalization = "NONE" ,
    transform = "LOG",
    analysis_method = "LM",
    output = "demo_output",
    fixed_effects = c("subject_type", "Age_years"))

diff_MC_age_results <- as.data.frame(diff_MC_age$results)
diff_MC_corr_MC_results <- diff_MC_age_results %>% filter(metadata == "subject_type")
diff_MC_corr_age_results <- diff_MC_age_results %>% filter(metadata == "Age_years")
write_xlsx(diff_MC_corr_MC_results, "/Users/fienvanbeveren/Documents/picu/results/vs controls/vs MC/diff_corr_MC.xlsx") 
write_xlsx(diff_MC_corr_age_results, "/Users/fienvanbeveren/Documents/picu/results/vs controls/vs MC/diff_corr_age.xlsx") 
```

# Save results
```{r}
saveRDS(diff_MC_corr_MC_results, here("data", "results DA", "control_MC.Rds"))
```


#.
# Select top 30 ASVs
```{r}
topn <- sort(rowMeans(otu_table(ps_young_RA)), decreasing = T)[1:30]

ps_top_30 <- ps_young_RA %>% 
    prune_taxa(taxa_names(.) %in% names(topn), .)

otu_top_30 <- otu_tab_to_df(ps_top_30) %>% column_to_rownames("#SampleID")
ASVs_top_30 <- colnames(otu_top_30)
setdiff(ASV_to_test, ASVs_top_30)
setdiff(ASVs_top_30, ASV_to_test)
```

#.
# Prepare data
```{r}
otu_RA <- otu_tab_to_df(ps_MC) %>% column_to_rownames("#SampleID")
meta_msl <- meta %>% column_to_rownames("#SampleID")
```

Porphyromonas_pasteri_75 -> 

# test 
```{r}
ps_PICU <- ps_comb %>% subset_samples (!environment == "control") %>% pres_abund_filter(pres=1, abun=0)
ps_PICU_RA <- ps_PICU %>% to_RA()
ps_young <- ps_PICU_RA %>% subset_samples(Age_years < 10) 
otu_RA_young <- otu_tab_to_df(ps_young) %>% column_to_rownames("#SampleID")
meta_PICU_young <- ps_young %>% meta_to_df() %>% column_to_rownames("#SampleID")
diff_t <-  Maaslin2(
    input_data = otu_RA_young, 
    input_metadata = meta_PICU_young,
    min_abundance = 0.002,
    min_prevalence = 0.1,
    normalization = "NONE" ,
    transform = "LOG",
    analysis_method = "LM",
    output = "demo_output",
    fixed_effects = c("Ward", "Age_years"))

diff_t_results <- as.data.frame(diff_t$results) 
diff_t_results %>%pull (feature)
```


# Run Maaslin wt different thresholds
## 1% RA in 10% of the samples
```{r}
diff_10 <-  Maaslin2(
    input_data = otu_RA, 
    input_metadata = meta_msl,
    min_abundance = 0.01,
    min_prevalence = 0.1,
    normalization = "NONE" ,
    transform = "LOG",
    analysis_method = "LM",
    output = "demo_output",
    fixed_effects = c("subject_type"))

diff_10_results <- as.data.frame(diff_10$results) 
diff_10_results
as.data.frame(diff_10$results) %>% count(qval < 0.25)
ASV_10 <- as.data.frame(diff_10$results) %>% pull(feature)
write_xlsx(diff_10_results, here("results/vs controls/vs MC/diff_10_results.xlsx"))
```
## 0.1% RA in 25% of the samples
```{r}
diff_25 <-  Maaslin2(
    input_data = otu_RA, 
    input_metadata = meta_msl,
    min_abundance = 0.001,
    min_prevalence = 0.25,
    normalization = "NONE" ,
    transform = "LOG",
    analysis_method = "LM",
    output = "demo_output",
    fixed_effects = c("subject_type"))

diff_25_results <- as.data.frame(diff_25$results) 
diff_25_results
as.data.frame(diff_25$results) %>% count(qval < 0.25)
ASVs_25 <- diff_25_results %>% pull(feature)
as.data.frame(diff_25$results) %>% pull(feature) %>% as.data.frame()
write_xlsx(diff_25_results, here("results/vs controls/vs MC/diff_25_results.xlsx"))

union(ASVs_25, ASV_10)
```

## 0.1% RA in 10% of the samples
```{r}
diff_10_2 <-  Maaslin2(
    input_data = otu_RA, 
    input_metadata = meta_msl,
    min_abundance = 0.001,
    min_prevalence = 0.1,
    normalization = "NONE" ,
    transform = "LOG",
    analysis_method = "LM",
    output = "demo_output",
    fixed_effects = c("subject_type"))

diff_10_2_results <- as.data.frame(diff_10_2$results) 
diff_10_2_results
as.data.frame(diff_10_2$results) %>% count(qval < 0.25)
ASV_10_2 <- as.data.frame(diff_10_2$results) %>% pull(feature) 
write_xlsx(diff_10_2_results, here("results/vs controls/vs MC/diff_10_2_results.xlsx"))
```

## 0.2% RA in 10% of the samples
```{r}
diff_10_3 <-  Maaslin2(
    input_data = otu_RA, 
    input_metadata = meta_msl,
    min_abundance = 0.002,
    min_prevalence = 0.1,
    normalization = "NONE" ,
    transform = "LOG",
    analysis_method = "LM",
    output = "demo_output",
    fixed_effects = c("subject_type"))

diff_10_3_results <- as.data.frame(diff_10_3 $results) 
diff_10_3_results
as.data.frame(diff_10_3$results) %>% count(qval < 0.25)
as.data.frame(diff_10_3$results) %>% pull(feature) %>% as.data.frame()
write_xlsx(diff_10_3_results, here("results/vs controls/vs MC/diff_10_3_results.xlsx"))
```


# Consideration
0.2% RA in 10% vd samples test je 23 ASVs
0.1% RAin 10% vd samples test je 31 ASVs dit zijn eigenlijk de beste voor de control-MC vergelijking (245 samples)
Voor de MC-IC vergelijking (126 samples, want alleen in cases en alleen in kinderen onder de 10 jaar omdat ik wil corrigeren voor lft) gebruik ik 0.1% in 25% vd samples en 0.1% in 10% vd samples maar in de control-MC vergelijking houd je dan veel te weinig ASVs over (13 ASVs worden dan maar getest). 

Als je 0.2% RA in 10% vd samples voor IC-MC doet dan test je 46 ASVs, is wel weer wat veel. Maar je krijgt wel interessante uitkomsten zoals dat Neisseria_19 en Porphyromonas_pasteri_75 met IC zijn geassocieerd. 

Moet ik 2 verschillende thresholds kiezen voor de vergelijkingen (MC-IC en control-MC respectievelijk) of moet ik per vergelijking kiezen wat de beste threshold is om ASVs te vergelijken? Moet ik voor mijn control-MC vergelijking meer dan 23 ASVs vergelijking, heb het geveol dat heel veel nu significant is doordat het relatief weinig ASVs zijn die ik test (gezien er nu 245 samples zijn en eerder 126). Maar weet niet zeker of dit het geval is. 

# plots
```{r}
ASVs_25 <- c(ASVs_25, "Residuals")

plot_RA_msl <- function(ps, ASV_names, n, r){
ps_plot <- ps_MC %>% 
  prune_taxa(ASV_names, .) 

p <- ps_plot %>% 
  prep_bar(n=n) %>% 
  ggplot(aes(x = subject_type, y = value, col= subject_type, fill = subject_type)) +
  scale_colour_manual (values = c("#7FC97F", "#BEAED4")) +
  scale_fill_manual(values = c("#7FC97F", "#BEAED4")) +
  geom_jitter(alpha = 0.8, width = 0.2) +
  geom_boxplot(alpha = 0.5, outlier.colour = NA) +
  facet_wrap(~ OTU, scales = "free_x", nrow =r)
return(p)
}

setdiff(ASVs_25, ASV_10)
plot_RA_msl(ps_MC, ASVs_25, n=11, r=2)
plot_RA_msl(ps_MC, ASV_10, n=9, r = 2)
plot_RA_msl(ps_MC, ASV_10_2,n=30, r= 3)
ASV_10
```



# Contaminant? 


```{r}
# eigenlijk tov blanks in PICU samples checken ipv tov controle samples. Want dat is juist wat je test. Kan ook de ziekte dan zijn die dit verklaard.
ps_MC %>% 
  prep_bar(n=20) %>% 
  filter(OTU == "*Moraxella* (9)") %>% ggplot(aes(x = factor (iso_run_nr), y = value)) +
  geom_jitter(alpha = 0.8, width = 0.2) +
  geom_boxplot(alpha = 0.5, outlier.colour = NA) +
  ggforce::facet_row(~study, scales = "free_x", space = "free")

ps_comb %>%
  to_RA() %>%
  prep_bar(n = 100) %>%
  filter(OTU == "*Actinomyces* (54)") %>%
  ggplot(aes(x = factor(iso_run_nr), y = value)) +
  geom_jitter(alpha = 0.8, width = 0.2) +
    geom_boxplot(alpha = 0.5, outlier.colour = NA) +
    ggforce::facet_row(~ study, scales = "free_x", space = "free")

ps_comb %>%
  to_RA() %>%
  prep_bar(n = 100) %>%
  filter(OTU == "*Lautropia mirabilis* (81)") %>%
  ggplot(aes(x = factor(iso_run_nr), y = value)) +
  geom_jitter(alpha = 0.8, width = 0.2) +
    geom_boxplot(alpha = 0.5, outlier.colour = NA) +
    ggforce::facet_row(~ study, scales = "free_x", space = "free")

ps_comb %>%
  to_RA() %>%
  prep_bar(n = 100) %>%
  filter(OTU == "*Abiotrophia defectiva* (76)") %>%
  ggplot(aes(x = factor(iso_run_nr), y = value)) +
  geom_jitter(alpha = 0.8, width = 0.2) +
    geom_boxplot(alpha = 0.5, outlier.colour = NA) +
    ggforce::facet_row(~ study, scales = "free_x", space = "free")

ps_comb %>%
  to_RA() %>%
  prep_bar(n = 100) %>%
  filter(OTU == "*Helcococcus* (197)") %>%
  ggplot(aes(x = factor(iso_run_nr), y = value)) +
  geom_jitter(alpha = 0.8, width = 0.2) +
    geom_boxplot(alpha = 0.5, outlier.colour = NA) +
    ggforce::facet_row(~ study, scales = "free_x", space = "free")

ps_comb %>%
  to_RA() %>%
  prep_bar(n = 100) %>%
  filter(OTU == "*Streptococcus* (8)") %>%
  ggplot(aes(x = factor(iso_run_nr), y = value)) +
  geom_jitter(alpha = 0.8, width = 0.2) +
    geom_boxplot(alpha = 0.5, outlier.colour = NA) +
    ggforce::facet_row(~ study, scales = "free_x", space = "free")
```

