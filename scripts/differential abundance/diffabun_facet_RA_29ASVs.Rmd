---
title: "Untitled"
author: "Fien van Beveren"
date: "2022-09-12"
output: html_document
---

# Load packages

```{r}
library(tidyverse)
library(magrittr)
library(here)
library(glue)
library(phyloseq)
library(cowplot)
library(haven)
library(Maaslin2)
library(gdata)
library(writexl)
library(ggtext)
library(EnhancedVolcano)

subdir_name <- "diffabun_facet_RA_29ASVs"

# set paths
knitr::opts_knit$set(root.dir=".", aliases=c(h = "fig.height", w = "fig.width", ow = "out.width"))
knitr::opts_chunk$set(dev=c('png', 'pdf'), 
                      fig.path=here("results", "figures", glue("{subdir_name}/")), dpi=300)
theme_set(theme_light()) 
```

```{r knit, echo=F, eval=FALSE}
rmarkdown::render(input = here("scripts", str_c(subdir_name, ".Rmd")), output_dir = here("results"))
```

# Load functions

```{r}
source(here("src", "utils.R")) # please see the src-folder for some custom functions used in this script.
source(here("src", "load_dada.R"))
source(here("src", "colour_scheme.R"))
```

## Functions differential abundance
```{r}
otu_tab_to_df <- function(ps, sample_name = "#SampleID") {
  otu_tab <- phyloseq::otu_table(ps)
  df <- otu_tab %>%
    t %>%
    data.frame(check.names = F) %>%
    tibble::rownames_to_column(sample_name)
  return(df)
}
```

# Load data

```{r}
ps_comb <- readRDS(here("data", "processed", "ps_comb.Rds")) # PICU & MOL data 
ps_RA <- ps_comb %>% to_RA() 

ps <- ps_RA %>% subset_samples (Age_years <7)
meta <- ps %>% meta_to_df()
```


# Select ASVs that are tested in the comparisons
```{r}
ps_29 <- ps_comb %>% 
  subset_samples(Age < 10) %>% 
  pres_abund_filter(pres=15.4, abun = 0.004)

ps_29 %>% meta_to_df() %>% count(subject_type)
```

# Facet plot
## RA top 15, age & PDC 
```{r}
excl_cols <- c("sample_id", colnames(phyloseq::sample_data(ps_29)))
  
ps_29 %>%
  subset_samples(!is.na(subject_type) & Age <7) %>%
  to_RA() %>%
  ps_to_df(sample_name = "sample_id") %>%
  tidyr::pivot_longer(-dplyr::all_of(excl_cols), names_to = "OTU", values_to = "value") %>%
  dplyr::mutate(OTU = format_OTU(.data$OTU) %>%
                  forcats::fct_inorder() %>%
                  forcats::fct_rev()) %>%
  dplyr::arrange(.data$sample_id) %>%
  dplyr::mutate(sample_id = forcats::fct_inorder(.data$sample_id))%>%
  ggplot(aes(x = Age_years, y = value, colour = subject_type)) +
  geom_point(alpha = 0.5, outlier.colour = NA)  +
  geom_smooth(method = lm) +
  facet_wrap(~OTU) +
   scale_y_log10() +
  labs(subtitle = "RA 29 ASVs tested with Maaslin in relation to age & study group")

ps_29 %>% 
  subset_samples(Age< 7) %>% 
  to_RA()%>% 
  prep_bar(n=15) %>%
  ggplot(aes(x = Age_years, y = value, colour = subject_type)) +
  geom_point(alpha = 0.5, outlier.colour = NA)  +
  geom_smooth(method = lm) +
  facet_wrap(~OTU) +
  scale_y_log10() +
  labs(subtitle = "RA top 15 with relation to age & study group")
```

## Boxplot

```{r}
ps_29 %>%
  subset_samples(!is.na(subject_type) & Age <7) %>%
  to_RA() %>%
  ps_to_df(sample_name = "sample_id") %>%
  tidyr::pivot_longer(-dplyr::all_of(excl_cols), names_to = "OTU", values_to = "value") %>%
  dplyr::mutate(OTU = format_OTU(.data$OTU) %>%
                  forcats::fct_inorder() %>%
                  forcats::fct_rev()) %>%
  dplyr::arrange(.data$sample_id) %>%
  dplyr::mutate(sample_id = forcats::fct_inorder(.data$sample_id))%>%
  ggplot(aes(x = subject_type, y = value, colour = subject_type)) +
  geom_boxplot(alpha = 0.5, outlier.colour = NA)  +
  facet_wrap(~OTU) +
   scale_y_log10() +
  labs(subtitle = "RA 29 ASVs tested with Maaslin in relation to age & study group")
```


