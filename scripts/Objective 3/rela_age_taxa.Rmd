---
title: "Untitled"
author: "Fien van Beveren"
date: "2022-09-07"
output: html_document
---

# Load packages 
```{r setup, include=FALSE}
library(tidyverse); library(here); library(glue); library(magrittr); library(table1)
library(data.table); library(RColorBrewer); library(tableone); library(phyloseq)
library(ggpubr); library(gdata); library (ggcorrplot); library(decontam); library(GGally); library(ggforce)

subdir_name <- "rela_age_taxa"

# set paths
knitr::opts_knit$set(root.dir=".", aliases=c(h = "fig.height", w = "fig.width", ow = "out.width"))
knitr::opts_chunk$set(dev=c('png', 'pdf'), 
                      fig.path=here("results", "figures", glue("{subdir_name}/")), dpi=300)
theme_set(theme_light()) 
```


# Load functions

```{r cars}
source(here("src", "utils.R")) 
source(here("src", "load_dada.R"))
```

# Load data
```{r}
ps_comb <- readRDS(here("data", "processed", "ps_comb.Rds"))
ps_RA <- ps_comb %>% to_RA()
```

#. 
#PREP DATA
## Filter for cases
```{r}
ps_PICU <- ps_comb %>% subset_samples(study == "PICU")
```
## Add Rhino_status variable
```{r}
meta_PICU_rhino <- ps_PICU %>% 
  meta_to_df() %>% 
  mutate(Rhino_status = if_else(Verwekker1_Viro == "Rhino", "Rhinovirus positive", 
                                if_else(Verwekker2_Viro == "Rhino", "Rhinovirus positive", 
                                        if_else(Verwekker3_Viro == "Rhino", "Rhinovirus positive", "Rhinovirus negative"))),
         Rhino_status = if_else(is.na(Any_virusPCR_yn) & is.na(Rhino_status), "Unknown", 
                                if_else(Rhino_status == "Rhinovirus positive", "Rhinovirus positive", "Rhinovirus negative")))  %>% 
  column_to_rownames("#SampleID")

meta_PICU_rhino$Rhino_status <- NAToUnknown(meta_PICU_rhino$Rhino_status, unknown= c("Rhinovirus negative"))
meta_PICU_rhino$Rhino_status <- unknownToNA(meta_PICU_rhino$Rhino_status, unknown= c("Unknown"))

meta_PICU_rhino %>% count(Rhino_status)

sample_data(ps_PICU) <- meta_PICU_rhino
```

#TAXA & AGE
## Dolosigranulum & age 
```{r}
#Only for PICU samples
ps_PICU %>% 
  to_RA() %>% 
  prep_bar(n = 15) %>%
  filter(OTU == "*Dolosigranulum pigrum* (4)") %>%
  ggplot(aes(x = Age_years, y = value, colour = Any_virusPCR_yn)) +
  geom_point(alpha = 0.5, outlier.colour = NA)  +
  labs(subtitle = "RA Dolosigranulum pigrum over age") +
  geom_smooth(method = lm) #DOLO geen duidelijke rela met lft. De positieve virus lijn lijkt voor elke leeftijd wel een wat hogere RA aan DOLO in de samples te hebben. Dus dit verklaar wss de bevinding. Kan dus niet volledig door lft. verklaard worden 

# DOLO / age / PDC
ps_PICU %>%
  to_RA() %>%
  prep_bar(n = 15) %>%
  filter(OTU == "*Dolosigranulum pigrum* (4)") %>%
  ggplot(aes(x = Age_years, y = value, colour = PDC_Cat_80)) +
  geom_point(alpha = 0.5, outlier.colour = NA) +
  geom_smooth(method = lm) +
  labs(subtitle = "RA Dolosigranulum pigrum over age")  #? Weet niet goed hoe ik dit moet interpreteren.
```

## Corynebacterium & age
```{r}
#COR10 / age / any virus
ps_PICU %>%
  to_RA() %>%
  prep_bar(n = 15) %>%
  filter(OTU == "*Corynebacterium* (10)") %>%
  ggplot(aes(x = Age_years, y = value, colour = Any_virusPCR_yn)) +
  geom_point(alpha = 0.5, outlier.colour = NA) +
  geom_smooth(method = lm)+
  labs(subtitle = "RA Corynebacterium (10) over age")  # COR10 loopt voor zowel virus neg als pos omhoog en gaat dus omhoog met de leeftijd. Lijnen divergeren wel dus rela COR10 en virus en leeftijd niet helemaal uit elkaar te trekken. NB: in univariabel model wel geass met leeftijd maar niet met virus. In bivariaat model (lft +any virus) heel erg met lft geassocieerd en een beetje met virus. 
```

## Staphylococcus & Age
```{r}
ps_PICU%>%
  to_RA() %>%
  prep_bar(n = 15) %>%
  filter(OTU == "*Staphylococcus* (2)") %>%
  ggplot(aes(x = Age_years, y = value, colour = Any_virusPCR_yn)) +
  geom_point(alpha = 0.5, outlier.colour = NA) +
  geom_smooth(method = lm) +
  labs(subtitle = "RA Staphylococcus(2) over age") 
```
# Facet top 15 RA per age
```{r}
topn <- sort(rowMeans(otu_table(ps_PICU)), decreasing = T)[1:15]

ps_top_15 <- ps_PICU %>% 
    prune_taxa(taxa_names(.) %in% names(topn), .)
```

## RA top 15, age & virus 
```{r}
excl_cols <- c("sample_id", colnames(phyloseq::sample_data(ps_top_15)))
  
ps_top_15 %>%
  subset_samples(!is.na(Any_virusPCR_yn)) %>%
  to_RA() %>%
  ps_to_df(sample_name = "sample_id") %>%
  tidyr::pivot_longer(-dplyr::all_of(excl_cols), names_to = "OTU", values_to = "value") %>%
  dplyr::mutate(OTU = format_OTU(.data$OTU) %>%
                  forcats::fct_inorder() %>%
                  forcats::fct_rev()) %>%
  dplyr::arrange(.data$sample_id) %>%
  dplyr::mutate(sample_id = forcats::fct_inorder(.data$sample_id))%>%
  ggplot(aes(x = Age_years, y = value, colour = Any_virusPCR_yn)) +
  geom_point(alpha = 0.5, outlier.colour = NA)  +
  geom_smooth(method = lm) +
  facet_wrap(~OTU) +
  labs(subtitle = "RA top 15 with relation to age & presence of any virus")
```

## RA top 15, age & rhinovirus 
```{r}
excl_cols <- c("sample_id", colnames(phyloseq::sample_data(ps_top_15)))
  
ps_top_15 %>%
  subset_samples(!is.na(Rhino_status)) %>%
  to_RA() %>%
  ps_to_df(sample_name = "sample_id") %>%
  tidyr::pivot_longer(-dplyr::all_of(excl_cols), names_to = "OTU", values_to = "value") %>%
  dplyr::mutate(OTU = format_OTU(.data$OTU) %>%
                  forcats::fct_inorder() %>%
                  forcats::fct_rev()) %>%
  dplyr::arrange(.data$sample_id) %>%
  dplyr::mutate(sample_id = forcats::fct_inorder(.data$sample_id))%>%
  ggplot(aes(x = Age_years, y = value, colour = Rhino_status)) +
  geom_point(alpha = 0.5, outlier.colour = NA)  +
  geom_smooth(method = lm) +
  facet_wrap(~OTU) +
  labs(subtitle = "RA top 15 with relation to age & presence of rhinovirus")
```

## RA top 15, age & PDC 
```{r}
excl_cols <- c("sample_id", colnames(phyloseq::sample_data(ps_top_15)))
  
ps_top_15 %>%
  subset_samples(!is.na(PDC_Cat_80)) %>%
  to_RA() %>%
  ps_to_df(sample_name = "sample_id") %>%
  tidyr::pivot_longer(-dplyr::all_of(excl_cols), names_to = "OTU", values_to = "value") %>%
  dplyr::mutate(OTU = format_OTU(.data$OTU) %>%
                  forcats::fct_inorder() %>%
                  forcats::fct_rev()) %>%
  dplyr::arrange(.data$sample_id) %>%
  dplyr::mutate(sample_id = forcats::fct_inorder(.data$sample_id))%>%
  ggplot(aes(x = Age_years, y = value, colour = PDC_Cat_80)) +
  geom_point(alpha = 0.5, outlier.colour = NA)  +
  geom_smooth(method = lm) +
  facet_wrap(~OTU) +
  labs(subtitle = "RA top 15 with relation to age & PDC categories")
```

```{r}
excl_cols <- c("sample_id", colnames(phyloseq::sample_data(ps_top_15)))
  
ps_top_15 %>%
  subset_samples(!is.na(Klachten_7dgn_JN)) %>%
  to_RA() %>%
  ps_to_df(sample_name = "sample_id") %>%
  tidyr::pivot_longer(-dplyr::all_of(excl_cols), names_to = "OTU", values_to = "value") %>%
  dplyr::mutate(OTU = format_OTU(.data$OTU) %>%
                  forcats::fct_inorder() %>%
                  forcats::fct_rev()) %>%
  dplyr::arrange(.data$sample_id) %>%
  dplyr::mutate(sample_id = forcats::fct_inorder(.data$sample_id))%>%
  ggplot(aes(x = Age_years, y = value, colour = Klachten_7dgn_JN)) +
  geom_point(alpha = 0.5, outlier.colour = NA)  +
  geom_smooth(method = lm) +
  facet_wrap(~OTU) +
  labs(subtitle = "RA top 15 with relation to age & long symptoms")
```





