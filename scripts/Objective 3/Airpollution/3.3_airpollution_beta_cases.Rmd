---
title: "3.3 air pollution beta cases"
author: "Fien van Beveren"
date: "2022-11-03"
output: html_document
---

# Load packages

```{r}
library(tidyverse)
library(magrittr)
library(here)
library(glue)
library(phyloseq)
library(vegan)
library(cowplot)
library(gdata)
library(ggtext)
library(RColorBrewer)

subdir_name <- "3.3_airpollution_beta_cases"

# set paths
knitr::opts_knit$set(root.dir=".", aliases=c(h = "fig.height", w = "fig.width", ow = "out.width"))
knitr::opts_chunk$set(dev=c('png', 'pdf'), 
                      fig.path=here("results", "figures", glue("{subdir_name}/")), dpi=300)
theme_set(theme_light()) 
```

```{r knit, echo=F, eval=FALSE}
rmarkdown::render(input = here("scripts", "Objective 3", "Airpollution", str_c(subdir_name, ".Rmd")), output_dir = here("results", "Objective 3", "Airpollution"))
```

# Load functions

```{r}
source(here("src", "utils.R")) # please see the src-folder for some custom functions used in this script.
source(here("src", "load_dada.R"))
source(here("src", "colour_scheme.R"))
source(here("src", "colour_scheme_2.R"))
source(here::here("src", "pcoa_veg.R"))
source(here::here("src", "plot_pcoa.R"))
```

# Load data 
## microbiota
```{r}
ps_comb <- readRDS(here("data", "processed", "ps_comb.Rds"))

ps_cases <- ps_comb %>% 
  to_RA() %>% 
  subset_samples(study == "PICU" & !Study_number == "P1312") %>%  # 1 NA in postcode data (for subject P1312
  pres_abund_filter(pres=1, abun=0)

meta_case <- ps_cases %>% 
  meta_to_df() %>% 
  dplyr::select(`#SampleID`, Study_number, study, Ward, Age_years)
```

## post-codes
```{r}
postcodes <- read.csv(here("metadata", "postcodes", "postcodes luchtvervuiling.csv"))
postcodes_case <- postcodes %>% filter(study == "PICU") %>% select(-X)
```

# Objectives
* Evaluate if the overal microbiota composition in all cases for different levels of NO2/PM25/PM10 exposures.  
* Combine postcode data to microbiota data
* create distance matrix
* create PCOA plot
* Perform PERMANOVA

# Combine postcodes to microbiota data
```{r}
meta_post <- meta_case %>% left_join(postcodes_case, by = "Study_number") %>% column_to_rownames("#SampleID")

sample_data(ps_cases) <- meta_post
```


# Create distance matrix

```{r}
otu_case <- otu_table(ps_cases)[order(rowSums(otu_table(ps_cases)), decreasing=T),] 

pcoa <- pcoa_veg(t(otu_case), method = "bray")

bc <- vegdist(t(otu_case), method = "bray")
```

# Plot PCOA
* NB: je kan bv PCOA-as 1 met NO2/PM25/PM10 en PCOA-as 2 met NO2/PM25/PM10 plotten. 
* Geen idee hoe ik dit moet doen. 
* Dit moet niet al te moeilijk zijn. 
* Eens denken of je dit kan klaar spelen -> want wel nice om misschien aan mensjes van STATIC-PRO te laten zien. 

```{r}

```

# PERMANOVA
# Univariable
## NO2
```{r}
permutation <- 1000
set.seed(100)
vegan::adonis2 (bc ~ NO2,
                            data = meta_post,
                            permutations = permutation,
                            method = "bray")
```
## PM25
```{r}
set.seed(100)
vegan::adonis2 (bc ~ PM25,
                            data = meta_post,
                            permutations = permutation,
                            method = "bray")
```
## PM10
```{r}
set.seed(100)
vegan::adonis2 (bc ~ PM10,
                            data = meta_post,
                            permutations = permutation,
                            method = "bray")
```

# Multivariable
With age in model

## NO2
```{r}
set.seed(100)
vegan::adonis2 (bc ~ NO2 +Age_years,
                data = meta_post,
                permutations = permutation,
                method = "bray", 
                by = "margin")
```
## PM25
```{r}
set.seed(100)
vegan::adonis2 (bc ~ PM25 + Age_years,
                data = meta_post,
                permutations = permutation,
                method = "bray",
                by = "margin")
```
## PM10
```{r}
set.seed(100)
vegan::adonis2 (bc ~ PM10 + Age_years,
                data = meta_post,
                permutations = permutation,
                method = "bray",
                by = "margin")
```

# Conclusion
Both univariable- & multivariable-model (wt age and the air pollution parameter) do not impact the overall microbiota of a child admitted to hospital with an asthma exacerbation. 