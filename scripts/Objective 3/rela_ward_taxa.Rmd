---
title: "Untitled"
author: "Fien van Beveren"
date: "2022-09-12"
output: html_document
---

# Load packages 
```{r setup, include=FALSE}
library(tidyverse); library(here); library(glue); library(magrittr); library(table1)
library(data.table); library(RColorBrewer); library(tableone); library(phyloseq)
library(ggpubr); library(gdata); library (ggcorrplot); library(decontam); library(GGally); library(ggforce)

subdir_name <- "rela_ward_taxa"

# set paths
knitr::opts_knit$set(root.dir=".", aliases=c(h = "fig.height", w = "fig.width", ow = "out.width"))
knitr::opts_chunk$set(dev=c('png', 'pdf'), 
                      fig.path=here("results", "figures", glue("{subdir_name}/")), dpi=300)
theme_set(theme_light()) 
```


# Load functions

```{r cars}
source(here("src", "utils.R")) 
source(here("src", "load_dada.R"))
source(here("src", "colour_scheme.R"))
```

# Load data
```{r}
ps_comb <- readRDS(here("data", "processed", "ps_comb.Rds"))
ps_RA <- ps_comb %>% to_RA()
```

#. 

# Objectives
Taxa & ward
Variables & Ward

#PREP DATA
## Filter for cases
```{r}
ps_PICU <- ps_comb %>% subset_samples(study == "PICU")
```
## Add Rhino_status variable
```{r}
meta_PICU_rhino <- ps_PICU %>% 
  meta_to_df() %>% 
  mutate(Rhino_status = if_else(Verwekker1_Viro == "Rhino", "Rhinovirus positive", 
                                if_else(Verwekker2_Viro == "Rhino", "Rhinovirus positive", 
                                        if_else(Verwekker3_Viro == "Rhino", "Rhinovirus positive", "Rhinovirus negative"))),
         Rhino_status = if_else(is.na(Any_virusPCR_yn) & is.na(Rhino_status), "Unknown", 
                                if_else(Rhino_status == "Rhinovirus positive", "Rhinovirus positive", "Rhinovirus negative")))  %>% 
  column_to_rownames("#SampleID")

meta_PICU_rhino$Rhino_status <- NAToUnknown(meta_PICU_rhino$Rhino_status, unknown= c("Rhinovirus negative"))
meta_PICU_rhino$Rhino_status <- unknownToNA(meta_PICU_rhino$Rhino_status, unknown= c("Unknown"))

meta_PICU_rhino %>% count(Rhino_status)

sample_data(ps_PICU) <- meta_PICU_rhino
```

#TAXA & WARD

## Dolosigranulum & Ward
```{r}
ps_PICU %>% 
  to_RA() %>% 
  prep_bar(n = 15) %>%
  filter(OTU == "*Dolosigranulum pigrum* (4)") %>%
  ggplot(aes(x = subject_type, y = value, colour = subject_type, fill = subject_type)) +
  geom_jitter(alpha = 0.3) +
  geom_boxplot(alpha = 0.5, outlier.colour = NA)  +
  labs(subtitle = "RA Dolosigranulum pigrum ward of admission") +
  scale_fill_manual(values = cols$ward_2) +
  scale_colour_manual(values = cols$ward_2)
```
## Corynebacterium 10 & Ward
```{r}
ps_PICU %>% 
  to_RA() %>% 
  prep_bar(n = 15) %>%
  filter(OTU == "*Corynebacterium* (10)") %>%
  ggplot(aes(x = subject_type, y = value, colour = subject_type, fill = subject_type)) +
  geom_jitter(alpha = 0.3) +
  geom_boxplot(alpha = 0.5, outlier.colour = NA)  +
  labs(subtitle = "RA Corynebacterium per ward of admission") +
   scale_fill_manual(values = cols$ward_2) +
  scale_colour_manual(values = cols$ward_2)
```
## Stafylokokken & Ward
```{r}
ps_PICU %>% 
  to_RA() %>% 
  prep_bar(n = 15) %>%
  filter(OTU == "*Staphylococcus* (2)") %>%
  ggplot(aes(x = subject_type, y = value, colour = subject_type, fill = subject_type)) +
  geom_jitter(alpha = 0.3) +
  geom_boxplot(alpha = 0.5, outlier.colour = NA)  +
  labs(subtitle = "RA Staphylococcus per ward of admission") +
   scale_fill_manual(values = cols$ward_2) +
  scale_colour_manual(values = cols$ward_2)
```
# HOST FACTORS & WARD
## Any virus & Ward
```{r}
ps_PICU %>% 
  meta_to_df() %>% 
  ggplot(aes(x = subject_type, colour = Any_virusPCR_yn, fill = Any_virusPCR_yn)) +
  geom_bar(alpha = 0.5, outlier.colour = NA)  +
  labs(subtitle = "Presence of any virus per ward of admission") +
   scale_fill_brewer(palette = "Set1") +
  scale_colour_brewer(palette = "Set1")
```

## Rhino & Ward
```{r}
ps_PICU %>% 
  meta_to_df() %>% 
  ggplot(aes(x = subject_type, colour = Rhino_status, fill = Rhino_status)) +
  geom_bar(alpha = 0.5, outlier.colour = NA)  +
  labs(subtitle = "Presence of rhinovirus per ward of admission") +
   scale_fill_brewer(palette = "Set1") +
  scale_colour_brewer(palette = "Set1")
```

## PDC & Ward
```{r}
ps_PICU %>% 
  meta_to_df() %>% 
  filter(!is.na(PDC_Cat_80)) %>% 
  ggplot(aes(x = subject_type, colour = PDC_Cat_80, fill = PDC_Cat_80)) +
  geom_bar(alpha = 0.5, outlier.colour = NA)  +
  labs(subtitle = "Presence of any virus per ward of admission") +
   scale_fill_brewer(palette = "Set1") +
  scale_colour_brewer(palette = "Set1")
```

