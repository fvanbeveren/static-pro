---
title: "Untitled"
author: "Fien van Beveren"
date: "2023-02-16"
output: html_document
---

# Load packages

```{r}
library(tidyverse); library(magrittr); library(here); library(glue);
library(phyloseq); library(decontam); library(ggforce); 
library(cowplot); library(haven); library(labelled); library(lubridate)
library(gdata); library(knitr); library(vegan); library(ggrepel)
library(car); library(fpc); library(RColorBrewer)
library(ggpubr); library(rstatix)
library(factoextra);library(ggprism); library(ggforce); library(xlsx);
library(corrplot); library(ggcorrplot); library(forestmodel); library(lmtest)

subdir_name <- "Cases_multivar_models"

# set paths
knitr::opts_knit$set(root.dir=".", aliases=c(h = "fig.height", w = "fig.width", ow = "out.width"))
knitr::opts_chunk$set(dev=c('png', 'pdf'), 
                      fig.path=here("results", "figures", glue("{subdir_name}/")), dpi=300)
theme_set(theme_light()) 
```

```{r knit, echo=F, eval=FALSE}
rmarkdown::render(input = here("scripts", str_c(subdir_name, ".Rmd")), output_dir = here("results"))
```

# Load functions

```{r}
source(here("src", "utils.R")) # please see the src-folder for some custom functions used in this script.
source(here("src", "load_dada.R"))
source(here::here("src", "calc_ellipses.R"))
source(here::here("src", "pcoa_veg.R"))
source(here::here("src", "plot_pcoa.R"))
source(here::here("src", "colour_scheme.R"))
```

# Load data

```{r}
ps_comb <- readRDS(here("data", "processed", "ps_comb.Rds"))
ps_PICU <- ps_comb %>% subset_samples (environment == "case")%>% pres_abund_filter(pres=1, abun=0)
ps_PICU_RA <- ps_PICU %>% to_RA() 
meta_PICU <- ps_PICU_RA %>% 
  meta_to_df() %>% 
   mutate(ICS_naive = fct_recode(Parents_ICS,
                                "No" = "Only in case of complaints",
                                "No" = "Yes, but started <1 week before this admission",
                                "No" = "Yes, started >1 week before this admission",
                                "No" = "Yes, but only with asthma complaints",
                                "Yes" = "No")) %>%
  column_to_rownames("#SampleID") 

sample_data(ps_PICU_RA) <- meta_PICU
```


# Objectives
Create model with variables that have univariable p<0.2 for PERMANOVA analysis

# Missings? 
Age_years \
Any_virusPCR_yn \
Smoking\
Klachten_7dgn_JN\
ICS_naive\

In total 162-92 = 70 missings for one of the above variables. Since PERMANOVA cannot deal with missings we need to exclude them before creating a Bray-curtis dissimilarity matrix. 
```{r}
ps_PICU_RA %>% meta_to_df() %>% count(Age_years)
ps_PICU_RA %>% meta_to_df() %>% count(Any_virusPCR_yn) #3NAs
ps_PICU_RA %>% meta_to_df() %>% count(Smoking) # 43 NAs
ps_PICU_RA %>% meta_to_df() %>% count(Klachten_7dgn_JN) # 3 NAs
ps_PICU_RA %>% meta_to_df() %>% count(ICS_naive) #34 NAs 

ps_PICU_RA_multi_noNA <- ps_PICU_RA %>% subset_samples(!is.na(Any_virusPCR_yn) & ! is.na(Smoking) & ! is.na (Klachten_7dgn_JN) & ! is.na(ICS_naive)) %>% pres_abund_filter(pres=1, abun=0)
```

#. 
# Prep data
```{r}
otu_RA_ord <- otu_table(ps_PICU_RA_multi_noNA)[order(rowSums(otu_table(ps_PICU_RA_multi_noNA)), decreasing=T),] 

pcoa_all <- pcoa_veg(t(otu_RA_ord), method = "bray")
bc <- vegdist(t(otu_RA_ord), method = "bray") 
meta_noNA <- ps_PICU_RA_multi_noNA %>%
  meta_to_df() %>% 
  column_to_rownames("#SampleID") 
```


# All drivers
```{r}
permutation <- 1000
set.seed(100)
multi.all <- vegan::adonis2(bc ~ Klachten_7dgn_JN + Smoking + Any_virusPCR_yn + Age_years + ICS_naive,
                                 data = meta_noNA,
                                 permutations = permutation, 
                                 by = "margin")
broom::tidy(multi.all)
```

#Only ICS
```{r}
ps_PICU_RA_ICS <- ps_PICU_RA %>% 
  subset_samples(!is.na(ICS_naive)) %>%
  pres_abund_filter(pres=1, abun=0)
meta_ICS <- ps_PICU_RA_ICS %>% 
  meta_to_df() %>% 
  column_to_rownames("#SampleID") 

otu_RA_ord <- otu_table(ps_PICU_RA_ICS)[order(rowSums(otu_table(ps_PICU_RA_ICS)), decreasing=T),] 

pcoa_all <- pcoa_veg(t(otu_RA_ord), method = "bray")
bc <- vegdist(t(otu_RA_ord), method = "bray") 

set.seed(100)
multi.ICS <- vegan::adonis2(bc ~ ICS_naive,
                                 data = meta_ICS,
                                 permutations = permutation, 
                                 by = "margin")
broom::tidy(multi.ICS)
```

